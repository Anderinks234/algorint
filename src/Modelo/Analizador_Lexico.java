/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.LinkedList;

/**
 *
 * @author Anderlinks234,
 */

/*
    El objetivo de esta clase es procesar la informacion para generar una lista de tokens para el procesamiento sintatico
 */
public class Analizador_Lexico {

    LinkedList<Estado> Estados;
    LinkedList<String> NombreTokens;
    Arista[][] m;
    Estado Inicial;

    /*
    Contrtructor pordefecto de esta clase
     */
    public Analizador_Lexico() {
        this.Estados = new LinkedList<>();
        this.NombreTokens = new LinkedList<>();
        m = new Arista[0][0];

    }

    /*
    Añade un token a la lista de tokens 
    Parametros de entrada: Nombre del nuevo token
     */
    public boolean AñadirToken(String token) {
        if (!NombreTokens.contains(token)) {
            NombreTokens.add(token);
            return true;
        }
        return false;
    }

    /*
    Configura el estado inicial del Automata
    Parametros de Entrada: Nombre del estado a configurar como inicial
     */
    public boolean AñadiInicial(String id) {
        if (Existe(id)) {
            Inicial = buscarEstado(id);
            return true;
        }
        return false;
    }

    /*
        Valida si existe un estado en el automata
        Parametros de entrada: Nombre de el del token a buscar
     */
    public boolean Existe(String id) {
        for (int i = 0; i < Estados.size(); i++) {
            if (Estados.get(i).getId() == id) {
                return true;
            }
        }
        return false;
    }

    /*
    Devuelve el estado con nombre id
    Parameros de entrada: Nombre del estado a buscar
     */
    public Estado buscarEstado(String id) {
        for (int i = 0; i < Estados.size(); i++) {
            if (Estados.get(i).getId() == id) {
                return Estados.get(i);
            }
        }
        return null;
    }

    /*Añade arista con su respectiva expresion
        Parametros de entrada: recibe el nombre del nodo inicio y fin de la conecion, ademas de la expresion reguar para
        validar la accion de la arista
     */
    public Boolean AñadirArista(String desde, String hasta, String Expresion) {
        if (Existe(desde) && Existe(hasta)) {
            Estado origen = buscarEstado(desde);
            Estado destino = buscarEstado(hasta);
            if (origen != null && destino != null) {
                m[Estados.indexOf(origen)][Estados.indexOf(destino)] = new Arista(Expresion);
                return true;
            }
        }
        return false;
    }

    /*
    Añade un estado A la lsita de estados mientras no este ya presente en el automata
    Parametros de entrada: Si el estado es aceptador, nombre del token asociado al estado, y el identificador del token
     */
    public Boolean AñadirEstado(boolean Final, String NombreToken, String id, String idToken) {
        if (NombreTokens.contains(NombreToken)) {
            if (!Existe(id)) {
                Estados.add(new Estado(Final, NombreToken, id, idToken));
                Redimencionar();
                return true;
            }
        }
        return false;
    }

    /*
    Muestra matriz de adyacencia
     */
    public void Ver() {

        for (int i = 0; i < m.length; i++) {
            System.out.print("\t" + (Estados.get(i).getId()));
        }
        System.out.println("");
        for (int i = 0; i < m.length; i++) {
            System.out.print((Estados.get(i).getId()) + "" + "\t");
            for (int j = 0; j < m.length; j++) {
                System.out.print((((m[i][j]) == null) ? "0" : (m[i][j]).getTextExpresion()) + "\t");
            }
            System.out.println("");
        }
    }

    //Redimienciona la matriz a la hora de agreagar un nuevo estado
    private void Redimencionar() {
        Arista[][] aux = new Arista[(m.length + 1)][(m.length + 1)];

        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m.length; j++) {
                aux[i][j] = this.m[i][j];
            }
        }
        this.m = aux;
    }

    /*
       Devuelve una lista de tokens correspondientes a el codigo ingresado, nevagando en el automata y a medida que avanza entre los estados genera
        los tokens correspondientes, es llamado a la hora compilar el codigo.
        Parametros de entrada:  El codigo que desea convertir en tokens
     */
    public LinkedList<Token> GenerarTonkens(String codigo) {
        LinkedList<Token> Tokens = new LinkedList<>();
        String[] lineas = codigo.split("\n");
        Estado actual = this.Inicial;
        for (int i = 0; i < lineas.length; i++) {

            String[] columna = lineas[i].split("[ ]+");
            if (!lineas[i].isEmpty()) {
                if (!(lineas[i].charAt(0) + "").equals("#")) {
                    System.out.println(columna);
                    for (int j = 0; j < columna.length; j++) {
                        String cadena = columna[j].trim();
                        int tamaño = 0;
                        while (cadena.length() > 0) {
                            boolean ok = false;
                            for (int k = 0; k < m.length && !ok; k++) {
                                if (m[Estados.indexOf(actual)][k] != null && tamaño < cadena.length()) {
                                    if (m[Estados.indexOf(actual)][k].Pertenece("" + cadena.charAt(tamaño))) {
                                        tamaño++;
                                        ok = true;
                                        actual = Estados.get(k);
                                    }
                                }
                            }
                            if (!ok) {
                                if (actual.Final) {
                                    String a = cadena.substring(0, tamaño).trim();
                                    if (!a.contains(" ")) {
                                        Tokens.add(new Token(actual.NombreToken, cadena.substring(0, tamaño).trim(), actual.idToken, i + 1, j));
                                        System.out.println("El estado que me creo fue " + actual.getId());
                                    }
                                }
                                cadena = cadena.substring(tamaño, cadena.length());
                                tamaño = 0;
                                actual = Inicial;
                            }
                        }

                    }
                }
            }
            Tokens.add(new Token("fl", "fl", "fl", i + 1, 0));
            System.out.println("");
        }
        for (int i = 0; i < Tokens.size(); i++) {
            System.out.println(Tokens.get(i).getNombre() + " : " + Tokens.get(i).getDato() + " --> " + Tokens.get(i).getIdToken() + "[" + Tokens.get(i).linea + "] [" + Tokens.get(i).columna + "]");

        }
        System.out.println("");
        for (int i = 0; i < Tokens.size(); i++) {
            System.out.print(Tokens.get(i).getNombre());

        }
        System.out.println("");
        return Tokens;
    }
}
