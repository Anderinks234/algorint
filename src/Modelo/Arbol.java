/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.awt.Point;
import java.io.Serializable;
import java.util.LinkedList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Anderlink234
 */
 
/*
	Esta clase servirá como instancia para la creación del árbol de 
	ejecucion
*/
public class Arbol implements Serializable {

    private LinkedList nuevosTokens;
    int contador;
	private Nodo Raiz;
    private JPanel panelArbol;
	
    public JPanel getPanelArbol() {
        return panelArbol;
    }

    public void setPanelArbol(JPanel panelArbol) {
        this.panelArbol = panelArbol;
    }

    

    public Arbol(JPanel PanelArbol) {
        this.panelArbol = panelArbol;
    }

    public Nodo getRaiz() {
        return Raiz;
    }

    public void setRaiz(Nodo Raiz) {
        this.Raiz = Raiz;
    }

}
