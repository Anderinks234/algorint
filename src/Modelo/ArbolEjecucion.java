/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.awt.Point;
import java.io.Serializable;
import java.util.LinkedList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Anderlink234
 */
 
 /*
	Esta clase servirá como instancia para la creación del árbol de 
	de ejecución que servirá para la creación de los ambientes
	de programación generados
 */
public class ArbolEjecucion implements Serializable {

    private NodoE Raiz;
    private JPanel panelArbol;
    public int contador;

	/*
		Constructor parametrizado de la clase
		Parametros de entrada: Jpanel para la posterior graficación
	*/
    public ArbolEjecucion(JPanel PanelArbol) {
        this.panelArbol = panelArbol;
        contador=2;
    }
    /*
		Agrega un nuevo nodo al árbol, empezando a analizar desde la raiz
		siendo recubridor
		Paramentros de entrada: Un nodo, y el identificador del padre
		para agregarlo como hijo
	*/
    public void Agregar(NodoE Nuevo, int padre) {
        Agregar2(this.Raiz, Nuevo, padre);
    }
	/*
		Metodo auxiliar de agregar, implementado para casos generales de los nodos
		Paramentros de entrada: Un nodo, y el identificador del padre
		para agregarlo como hijo
	*/
    public void Agregar2(NodoE Nodo, NodoE Nuevo, int padre) {
      
        if (Nodo.getDato().getIdentificador() == padre) {
            Nodo.getHijos().add(Nuevo);
           
        }
        else {
            for (int i = 0; i < Nodo.getHijos().size(); i++) {
                Agregar2(Nodo.getHijos().get(i), Nuevo, padre);
            }
        }
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }

	/*
		Permite ver en la cosola del programador el árbol,
		iniciando desde la raiz
	*/
    public void ver() {
        ver2(this.Raiz);
    }
	/*
		Método auxiliar a ver(), que permite recorrer de forma
		recursiva y en inOrden
		Paramentros de entrada: Un nodo que contedrá a sus respectivos hijos
	*/
    public void ver2(NodoE Nodo) {
        System.out.println(Nodo.getDato().identificador);
        System.out.println(Nodo.getDato().verScope());
        for (int i = 0; i < Nodo.getHijos().size(); i++) {
            ver2(Nodo.getHijos().get(i));
        }
    }
    public JPanel getPanelArbol() {
        return panelArbol;
    }

    public void setPanelArbol(JPanel panelArbol) {
        this.panelArbol = panelArbol;
    }

    public NodoE getRaiz() {
        return Raiz;
    }

    public void setRaiz(NodoE Raiz) {
        this.Raiz = Raiz;
    }
}
