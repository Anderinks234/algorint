package Modelo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Anderlinks234
 */
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/*
    Esta clase genera un objeto que va incrustrado en cada una de las posiciones de la matriz donde hay una conexion haciendo
    de arista en el grafo
*/
public class Arista {

    Pattern Expresion;
    String textExpresion;

    public Arista() {

    }

    public Arista(String Expresion) {
        this.Expresion = Pattern.compile(Expresion);
        this.textExpresion = (Expresion);
    }

    /*  Valida la expresion regular incrustada en la arista si cumple con la validacion de dicha expresion regular
        podra avanzar en el automata generando tokens
        Parametros de entrada: Cadena a validar, por lo general es un caracter ya que el codigo se valida caracter por caracter
    */
    public boolean Pertenece(String cadena) {
        Matcher Resultado = this.Expresion.matcher(cadena);
        if (Resultado.matches()) {
            return true;
        }
        return false;
    }

    public Pattern getExpresion() {
        return Expresion;
    }

    public void setExpresion(Pattern Expresion) {
        this.Expresion = Expresion;
    }

    public String getTextExpresion() {
        return textExpresion;
    }

    public void setTextExpresion(String textExpresion) {
        this.textExpresion = textExpresion;
    }

}
