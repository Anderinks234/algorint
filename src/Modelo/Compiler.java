/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.LinkedList;
import javax.script.Invocable;
import javax.script.ScriptException;

/**
 *
 * @author Ander
 */

/*
    Esta clase es la encargada de de generar los ambientes de ejecucion que sean requeridos en un codigo javascript, almacena una lista de funciones
    y la que es requerida la busca y la ejectuta.
 */
public class Compiler {

    final LinkedList<Script> ListFunction;
    GodClass godclass;

    /*
    Contrusctor parametrizado
    Parametros de entrada: Una instancia de Goodclas, y La lista de Scripts (funciones)
     */
    public Compiler(LinkedList<Script> ListFuntion, GodClass C) {
        this.ListFunction = ListFuntion;
        this.godclass = C;

    }

    /*
    Este metodo es uno de los mas importantes en cuanto a invocacion de metodos concierne, ya que invoca la subrutina que previamente llamada
    desde el psudocodigo, y que ya esta almacenada en nuestra lista de scripts (funciones) creando una nueva instancia de este misma y ejecutandola,
    Retorna lo que el objeto arroje, es decir que si la funcion invocada devuelve un numero  este se le retorna a quien lo invovo, es de vital importancia
    comprender que esta subrutina es invocada directamente desde el codigo Javascript en ejecucion.
    Parametros de entrada: El script invocador (Subrurina en ejecucion actual), invocador( Nombre de la subrutina en ejecucion actual),Invicado(nombre de la
    subrutina que fue invocada desde el ambiente invocador), parametros(Lista de nombres de parametros en la entrada para luego pasarlos al scope de la nueva subrutina a ejecutar)
        
     */
    public Object invoke(Object scriptCopiainvocador, String invocador, String invocado, String parametros) throws ScriptException, NoSuchMethodException, CloneNotSupportedException {
        Script from = null;
        Script to = null;

        for (int i = 0; i < ListFunction.size(); i++) {
            if (ListFunction.get(i).getName().equals(invocador)) {
                // from = godclass.CreateScript(ListFunction.get(i).getTokens()); 
                from = (Script) scriptCopiainvocador;

            }
            if (ListFunction.get(i).getName().equals(invocado)) {
                to = godclass.CreateScript(ListFunction.get(i).getTokens());
                int identificador = godclass.getInterfaz().getArbol().getContador();
                to.setIdentificador(identificador);
                godclass.getInterfaz().getArbol().setContador(identificador + 1);
                NodoE Nuevo = new NodoE(to.getName(), to, godclass.getInterfaz().getPanelEjecucion());
                godclass.getInterfaz().getPanelEjecucion().add(Nuevo);
                godclass.getInterfaz().getArbol().Agregar(Nuevo, from.identificador);

            }
        }
        if (from == null) {
            if (to != null) {
                to.SimpleRun();
            }
        }
        else {
            return from.ComplexRun(to, parametros);
        }
        return null;
    }

}
