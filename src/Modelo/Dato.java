/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.LinkedList;

/**
 *
 * @author Ander
 */
 
 /*
	Permite manejar la estructura e informacion de los datos 
	de cada una de las variables generadas por el codigo escrito
	por el usuario
 */
public class Dato {

    //Para tener mas control sobre los tipos de datos, no tener que manejar datos genericos, si no mas bien manejar el dato semejante en java
    private String tipoDato;
    private Object Dato;
    private String identificador;

    /*Próximamente las estructuras de datos tambien que se van a utilizar */
    Dato(String tipoDato, String identificador) {
        this.tipoDato = tipoDato;
        this.identificador = identificador;
    }

    public String getTipoDato() {
        return tipoDato;
    }

    public void setTipoDato(String tipoDato) {
        this.tipoDato = tipoDato;
    }

    public Object getDato() {
        return Dato;
    }

    public void setDato(Object Dato) {
        this.Dato = Dato;
    }

	/*
		Retorna la informacion del dato actual, especificando el tipo,
		su nombre y su valor
	*/
    public String mostrarInfo() {

        if (tipoDato.equals("int")) {
            return "" + Dato + " : " + identificador + " : " + tipoDato;
        }
        if (tipoDato.equals("double")) {
            return "" + Dato + " : " + identificador;
        }
        if (tipoDato.equalsIgnoreCase("string")) {
            return (String) Dato + " : " + identificador;
        }
        if (tipoDato.equals("boolean")) {
            return "" + (boolean) Dato + " : " + identificador;
        }
        if (tipoDato.equals("Array int")) {
            return "" + (LinkedList<Integer>) Dato + " : " + identificador;
        }
        if (tipoDato.equals("Array double")) {
            return "" + (LinkedList<Double>) Dato + " : " + identificador;
        }
        if (tipoDato.equals("Array String")) {
            return "" + (LinkedList<String>) Dato + " : " + identificador;
        }
        if (tipoDato.equals("Array boolean")) {
            return "" + (LinkedList<String>) Dato + " : " + identificador;
        }
        if (tipoDato.equals("listaInt") || tipoDato.equals("pilaInt") || tipoDato.equals("colaInt")) {
            return "" + (LinkedList<Integer>) Dato + " : " + identificador + " : " + tipoDato;
        }
        if (tipoDato.equals("listaDouble") || tipoDato.equals("pilaDouble") || tipoDato.equals("colaDouble")) {
            return "" + (LinkedList<Double>) Dato + " : " + identificador + " : " + tipoDato;
        }
        if (tipoDato.equals("listaString") || tipoDato.equals("pilaString") || tipoDato.equals("colaString")) {
            return "" + (LinkedList<String>) Dato + " : " + identificador + " : " + tipoDato;
        }
        if (tipoDato.equals("listaBoolean") || tipoDato.equals("pilaBoolean") || tipoDato.equals("colaBoolean")) {
            return "" + (LinkedList<Boolean>) Dato + " : " + identificador + " : " + tipoDato;
        }
        return "";
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

}
