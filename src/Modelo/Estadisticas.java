/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.awt.Point;
import java.util.TreeMap;
import javax.swing.JFrame;
import org.jfree.*;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author Ander
 */
/*
    Clase encargada de procesar la informacion, tales como tiempo vs tamaño en la entrada de datos para mostrarlo de una manera grafica
 */
public class Estadisticas {

    /*
            Algorimo 1 --> Mapa (tamaño de entrada vs tiempo)
                       --> Mapa (tamaño de entrada vs numero de lineas)
            
            
            Algoritmo 2
            
     */
    JFrame Ventana = new JFrame();
    TreeMap<String, TreeMap<Integer, Point>> Informacion;
    DefaultCategoryDataset data;
    DefaultCategoryDataset data2;
    JFreeChart grafica;
    JFreeChart grafica2;

    public Estadisticas() {
    }

    public TreeMap<String, TreeMap<Integer, Point>> getInformacion() {
        return Informacion;
    }

    public void setInformacion(TreeMap<String, TreeMap<Integer, Point>> Informacion) {
        this.Informacion = Informacion;
    }

    public Estadisticas(TreeMap<String, TreeMap<Integer, Point>> Informacion) {
        this.Informacion = Informacion;
    }

    /*
    Recorre el diccionario de informacion almacenada correspondiente a la muestras tomadas previamente en cada ejecucion 
    Haciedo uso de la libreria mencionada anteriormente se le envia las cordenadas de las muestras y se grafican 
     */
    public void Graficar(String nombre) {
        data = new DefaultCategoryDataset();
        data2 = new DefaultCategoryDataset();

        //data.addValue(tiempo#lineas,Etiqueta, Etiqueta);
        TreeMap<Integer, Point> Mapa = new TreeMap<>();
        for (String id : Informacion.keySet()) {
            if (id.equalsIgnoreCase(nombre)) {
                Mapa = Informacion.get(nombre);
            }
            for (int prueba : Mapa.keySet()) {
                data.addValue(Mapa.get(prueba).x, prueba + "", "" + prueba);
            }
        }
        for (String id : Informacion.keySet()) {
            if (id.equalsIgnoreCase(nombre)) {
                Mapa = Informacion.get(nombre);
            }
            for (int prueba : Mapa.keySet()) {
                data2.addValue(Mapa.get(prueba).y, prueba + "", "" + prueba);
            }
        }
        grafica = ChartFactory.createBarChart3D("Identificador de Muestra Vs Numero de Lineas", "Identificador de Muestra", "Número de lineas", data, PlotOrientation.VERTICAL, true, true, false);
        grafica2 = ChartFactory.createBarChart3D("Identificador de Muestra  Vs Tiempo", "Identificador de Muestra", "Tiempo", data2, PlotOrientation.VERTICAL, true, true, false);
    }

    /*
        Muestra la grafica
        de tiempo vs Rnumeor de lienas
     */
    public void Mostrar1() {
        ChartPanel contenedor = new ChartPanel(grafica);
        Ventana = new JFrame("Rendimiento");
        Ventana.getContentPane().add(contenedor);
        Ventana.pack();
        Ventana.setVisible(true);

    }

    /*
        Muestra la grafica de
        numero de lienas vs Tiempo
     */
    public void Mostrar2() {
        ChartPanel contenedor = new ChartPanel(grafica2);
        Ventana = new JFrame("Rendimiento");
        Ventana.getContentPane().add(contenedor);
        Ventana.pack();
        Ventana.setVisible(true);
    }
}
