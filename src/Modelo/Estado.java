/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/*
	Unidad mínima del automata del analizador lexico
	donde se contiene información clave para la generacion
	de los tokens
*/
public class Estado {
   private String id;
   Boolean Final;
   String NombreToken;
   String idToken;
   
   /*
	Contructor por defecto de la clase
   */
   public Estado(){
       
   }
   /*
		Constructor parametrizado de la clase
		Parametros de entrada: final, token,idtoken,id
		que denotan las caracteristicas de cada nodo
   */
public Estado(boolean Final,String Token,String idToken, String id){
       this.id=id;
       this.Final=Final;
       this.NombreToken=Token;
       this.idToken=idToken;
}

    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    public Boolean getFinal() {
        return Final;
    }

    public void setFinal(Boolean Final) {
        this.Final = Final;
    }

    public String getNombreToken() {
        return NombreToken;
    }

    public void setNombreToken(String NombreToken) {
        this.NombreToken = NombreToken;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

 
   
    
}
