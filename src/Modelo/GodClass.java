/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.text.Normalizer;
import java.util.LinkedList;
import javax.script.Bindings;
import javax.script.Invocable;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import javax.swing.JFrame;
import javax.swing.JTextPane;
import Vista.*;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.JOptionPane;

/**
 *
 * @author Ander
 */
 /*
	Clase que será de gran importacia para el manejo de los
	pseudo códigos que ingresan los actores del sistema
 */
public class GodClass {

    LinkedList<Script> ListFunction;
    LinkedList<LinkedList<Token>> Funciones;
    Compiler C;
    String functionName;
    public Editor Interfaz;
    String LineaAux;
    int tokenActual = 0;
	/*
		Constructor parametrizado de la clase
		Parametros de entrada: Lista de funciones
		que contiene cada uno de los tokens agrupados por 
		funciones procedimientos
	*/
    public GodClass(LinkedList<LinkedList<Token>> Funciones) {
        this.Funciones = Funciones;
        functionName = "";
        LineaAux = "";
        ListFunction = new LinkedList<>();
    }
	/*
		Permite obtener cada una de las variables que son propias
		de un ambiente de codigo
		Paramentros de entrada: El nombre del método a obtener variables
	*/
    public String[] ObtenerVariablesMetodos(String cadena) {
        String sub[] = cadena.split("(\\(|\\))");
        cadena = sub[1];
        String variablesMetodos[] = cadena.split("\\,");
        for (int i = 0; i < variablesMetodos.length; i++) {
            System.out.println(variablesMetodos[i]);
        }
        return variablesMetodos;
    }
	/*
		Permite configurar los métodos que no tienen parametros de entrada
		Creando una copia del script
		Paramentros de entrada: El nombre de la funcion  que se procesará
	*/
    public void Run(String funcion) throws ScriptException, NoSuchMethodException, CloneNotSupportedException {
        for (int i = 0; i < ListFunction.size(); i++) {
            if (funcion.equals(ListFunction.get(i).getName())) {
                //ListFunction.get(i).SimpleRun();
                Script run = CreateScript(ListFunction.get(i).getTokens());
                run.setIdentificador(1);
                NodoE nuevo = new NodoE(run.getName(), run, Interfaz.getPanelEjecucion());
                Interfaz.getArbol().setRaiz(nuevo);
                this.getInterfaz().getPanelEjecucion().add(nuevo);
                run.SimpleRun();
            }
        }

        for (int i = 0; i < ListFunction.size(); i++) {
            ListFunction.get(i).update();
            ListFunction.get(i).verScope();
            }

    }
	/*
		Procesa y modifica los tokens que contengan cadenas para evitar
		problemas de orden sintactico
		Paramentros de entrada: Lista de tokens
	*/
    public LinkedList<Token> ModificarStirng(LinkedList<Token> Tokens) {
        for (int i = 0; i < Tokens.size(); i++) {

            if (Tokens.get(i).getNombre().equals("¢")) {
                int con = i + 1;
                String Dto = "" + "'";
                if (con <= Tokens.size()) {
                    while (!Tokens.get(con).getNombre().equals("¢")) {
						Dto += " " + Tokens.get(con).getDato();
                        Tokens.remove(con);
                                            }
                    Dto += "'";
                }

                Token nuevo = new Token("¤", Dto, "cadena", Tokens.get(i).getLinea(), Tokens.get(i).getColumna());
                Tokens.add(con, nuevo);
                Tokens.remove(con - 1);
                Tokens.remove(con);

            }

        }
        return Tokens;
    }
	/*
		Recorre toda la lista de tokens para crear un script
		recolectando la informacion necesaria como lo es el
		scope, su correspondiente código en javascript y las
		herramientas (Libreria engine) para la ejecucion de su
		codigo
		Paramentros de entrada: Lista de tokens
		
	*/
    public Script CreateScript(LinkedList<Token> Tokens) {

        Tokens = ModificarStirng(Tokens);
        LinkedList<Token> Copia = new LinkedList<>();
        for (int i = 0; i < Tokens.size(); i++) {
            Token Nuevo = new Token();
            Nuevo.setColumna(Tokens.get(i).getColumna());
            Nuevo.setDato(Tokens.get(i).getDato());
            Nuevo.setIdToken(Tokens.get(i).getIdToken());
            Nuevo.setLinea(Tokens.get(i).getLinea());
            Nuevo.setNombre(Tokens.get(i).getNombre());
            Copia.add(Nuevo);
        }
        Script nuevo = new Script();
        configureScope(Copia, nuevo);
        //String Codejs = traductionCode(Tokens);
        String Codejs = TraducirCodigo(Copia);
        System.out.println("Codigo que esta llegando" + Codejs);
        nuevo.setCode(Codejs);
        nuevo.setName(functionName);
        Compiler compilador = new Compiler(ListFunction, this);
        nuevo.setComp(compilador);
        nuevo.update();
        nuevo.setTokens(Tokens);
        nuevo.setGod(this);
        nuevo.setInterfaz(Interfaz);
        return nuevo;
    }
	/*
		Crea cada una de las funciones que le entran como paramentros
		representadas en grupos de tokens y ejecuta la funcion createScrips
		por cada uno de los grupo
	*/
    public void createScrips() {

        for (int i = 0; i < Funciones.size(); i++) {

            ListFunction.add(CreateScript(Funciones.get(i)));
            functionName = "";
        }

        Compiler compilador = new Compiler(ListFunction, this);
        for (int i = 0; i < ListFunction.size(); i++) {
            ListFunction.get(i).setComp(compilador);
            // System.out.println("... " + ListFunction.get(i).getCode());
            ListFunction.get(i).update();
            ListFunction.get(i).verScope();
            System.out.println("-------------------------------------");
        }

    }
	/*
		Agrega a un mapa el número de la linea en el editor y un string
		con el correspondiente codigo real asociado
		Paramentros de entrada: Lista de tokens
	*/
    public TreeMap<Integer, String> LineasSintraducir(LinkedList<Token> ListaTokens) {
        String Linea = "";
        TreeMap<Integer, String> Diccionario = new TreeMap<Integer, String>();
        for (int i = 0; i < ListaTokens.size(); i++) {
            if (ListaTokens.get(i).getIdToken().equalsIgnoreCase("fl")) {
                Diccionario.put(ListaTokens.get(i).getLinea(), Linea);
                Linea = "";
            }
            else {
                Linea += " " + ListaTokens.get(i).getDato();
            }
        }
        return Diccionario;
    }
	/*
		Recorre cada uno de los tokens detectando y traduciendo a la declaracion
		de variables, con su tipo y nombre a lenguaje javascript
		Paramentros de entrada: Lista de tokens
	*/
    public LinkedList<Token> TraducirTipos(LinkedList<Token> LisaTokens) {
        LinkedList<String> tipos = new LinkedList<>();

        tipos.add("string");
        tipos.add("double");
        tipos.add("int");
        tipos.add("boolean");
        boolean comp = false;
        for (int i = 0; i < LisaTokens.size(); i++) {
            if (LisaTokens.get(i).getIdToken().equalsIgnoreCase("cadena")) {
                LisaTokens.get(i).setDato("'" + LisaTokens.get(i).getDato() + "'");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("procedure")) {
                LisaTokens.get(i).setDato("function");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("begin")) {
                LisaTokens.get(i).setDato("{");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("end")) {
                LisaTokens.get(i).setDato("}");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("E")) {
                LisaTokens.get(i).setDato("");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("ES")) {
                LisaTokens.get(i).setDato("");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("or")) {
                LisaTokens.get(i).setDato("||");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("and")) {
                LisaTokens.get(i).setDato("&&");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("=")) {
                LisaTokens.get(i).setDato("==");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("<-")) {
                LisaTokens.get(i).setDato("=");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("<>")) {
                LisaTokens.get(i).setDato("!=");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("do")) {
                LisaTokens.get(i).setDato("");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("T")) {
                LisaTokens.get(i).setDato("true");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("F")) {
                LisaTokens.get(i).setDato("false");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("then")) {
                LisaTokens.get(i).setDato("");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("to")) {
                LisaTokens.get(i).setDato("<");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("downto")) {
                LisaTokens.get(i).setDato(">");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("repeat")) {
                LisaTokens.get(i).setDato("do{");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("int") && !LisaTokens.get(i + 1).getDato().equalsIgnoreCase("[")) {
                LisaTokens.get(i).setDato("");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("double") && !LisaTokens.get(i + 1).getDato().equalsIgnoreCase("[")) {
                LisaTokens.get(i).setDato("");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("boolean") && !LisaTokens.get(i + 1).getDato().equalsIgnoreCase("[")) {
                LisaTokens.get(i).setDato("");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("string") && !LisaTokens.get(i + 1).getDato().equalsIgnoreCase("[")) {
                LisaTokens.get(i).setDato("");
            }
            if (LisaTokens.get(i).getDato().equalsIgnoreCase("")) {
                LisaTokens.get(i).setDato("");
            }

        }
        return LisaTokens;
    }
	/*
		Se encarga de traducir los métodos propios del usuario  y que 
		no estan registrados como métodos especiales que ofrece el programa
		Representado en formato y estructura javascript
		Paramentros de entrada: Lista de tokens
	*/
    public String TraducirMetodos(LinkedList<Token> ListaTokens) {
        String linea = "";

        linea += "Comp.invoke( script,'" + functionName + "' , '" + ListaTokens.get(tokenActual).getDato() + "','";
        if (ListaTokens.get(tokenActual + 2).getIdToken().equals(")")) {
            linea += "')";
            tokenActual = tokenActual + 3;
        }
        else {
            tokenActual = tokenActual + 2;
            while (!ListaTokens.get(tokenActual).getDato().equals(")")) {
                if (ListaTokens.get(tokenActual).getIdToken().equals("id")) {
                    linea += ListaTokens.get(tokenActual).getDato() + ",";
                }
                tokenActual++;
            }

            linea = linea.substring(0, linea.length() - 1);
            linea += "')";
        }
        return linea;
    }
	/*
	Es la funcion encargada de tomar el pseudocodigo ingresado por 
	el usuario y trasnformar o generar su estructura equivalente en 
	lenguaje javascript
	*/
    public String TraducirCodigo(LinkedList<Token> ListaTokens) {
        ListaTokens = ModificarStirng(ListaTokens);

        TreeMap<Integer, String> LineasReales = LineasSintraducir(ListaTokens);
        ListaTokens = TraducirTipos(ListaTokens);
        LinkedList<String> Lineas = new LinkedList<>();
        tokenActual = 0;
        boolean retorno = false;
        LinkedList<String> tipos = new LinkedList<>();
        tipos.add("string");
        tipos.add("double");
        tipos.add("int");
        tipos.add("boolean");
        while (tokenActual < ListaTokens.size()) {
            String LineaActual = "";
            if (ListaTokens.get(tokenActual).getDato().equalsIgnoreCase("int") || ListaTokens.get(tokenActual).getDato().equalsIgnoreCase("double") || ListaTokens.get(tokenActual).getDato().equalsIgnoreCase("string") || ListaTokens.get(tokenActual).getDato().equalsIgnoreCase("boolean")) {
                System.out.println("Entre<------------------------------------------------------");
                if (ListaTokens.get(tokenActual + 1).getDato().equals("[")) {
                    String tipo = ListaTokens.get(tokenActual).getIdToken();
                    System.out.println("tipo:   " + tipo);
                    LineaActual += "var xxx =";
                    tokenActual += 2;
                    while (!ListaTokens.get(tokenActual).getIdToken().equals("]")) {

                        LineaActual += ListaTokens.get(tokenActual).getDato();
                        tokenActual = tokenActual + 1;
                    }
                    String id = ListaTokens.get(tokenActual + 1).getDato();
                    String dato = "";
                    if (tipo.equalsIgnoreCase("int") || tipo.equalsIgnoreCase("double")) {
                        dato = "0";
                    }
                    if (tipo.equalsIgnoreCase("string")) {
                        dato = "''";
                    }
                    if (tipo.equalsIgnoreCase("boolean")) {
                        dato = "false";
                    }

                    LineaActual += "; for(var i=0; i < xxx ; i++){" + id + ".add(" + dato + "); }";

                    LineaAux = "script.stop(" + '"' + LineasReales.get(ListaTokens.get(tokenActual).getLinea()) + '"' + ", " + ListaTokens.get(tokenActual).getLinea() + ");";
                    Lineas.add(LineaAux);
                    Lineas.add(LineaActual);
                }

            }
            if (ListaTokens.get(tokenActual).getDato().equalsIgnoreCase("return")) {
                LineaActual = "";
                
                while (!ListaTokens.get(tokenActual).getDato().equalsIgnoreCase("fl")) {
                    LineaActual += " " + ListaTokens.get(tokenActual).getDato();
                    tokenActual++;
                }
                Lineas.add("script.estado(" + '"' + "inactivo" + '"' + ");");
                LineaAux = "script.stop(" + '"' + LineasReales.get(ListaTokens.get(tokenActual).getLinea()) + '"' + ", " + ListaTokens.get(tokenActual).getLinea() + ");";
                Lineas.add(LineaAux);
                Lineas.add(LineaActual + ";");
                retorno = true;
            }

            if (ListaTokens.get(tokenActual).getDato().equalsIgnoreCase("{")) {
                Lineas.add(ListaTokens.get(tokenActual).getDato());
                Lineas.add(LineaAux);
                LineaAux = "";
            }
            if (ListaTokens.get(tokenActual).getDato().equalsIgnoreCase("}")) {
                Lineas.add(ListaTokens.get(tokenActual).getDato());
                LineaAux = "";
            }
            if (ListaTokens.get(tokenActual).getDato().equalsIgnoreCase("function")) {
                LineaActual = "";
                functionName = ListaTokens.get(tokenActual + 1).getDato();
                boolean flag = false;
                while (!ListaTokens.get(tokenActual).getDato().equalsIgnoreCase("fl")) {
                    if (!ListaTokens.get(tokenActual).getDato().equals("int") && !ListaTokens.get(tokenActual).getDato().equals("double") && !ListaTokens.get(tokenActual).getDato().equals("[") && !ListaTokens.get(tokenActual).getDato().equals("]") && !ListaTokens.get(tokenActual).getDato().equals("string") && !ListaTokens.get(tokenActual).getDato().equals("boolean")) {
                        LineaActual += " " + ListaTokens.get(tokenActual).getDato();
                    }
                    if (ListaTokens.get(tokenActual).getDato().equalsIgnoreCase("{")) {

                        flag = true;
                    }
                    tokenActual++;
                }

                Lineas.add(LineaActual);
                LineaAux = "script.estado(" + '"' + "activo" + '"' + ");" + "script.stop(" + '"' + LineasReales.get(ListaTokens.get(tokenActual).getLinea()) + '"' + ", " + ListaTokens.get(tokenActual).getLinea() + ");";
                if (flag) {

                    Lineas.add(LineaAux);
                    Lineas.add("script.estado(" + '"' + "activo" + '"' + ");");
                    LineaAux = "";
                }
            }
            if (ListaTokens.get(tokenActual).getIdToken().equals("for")) {
                LineaActual = "";
                boolean flag = false;
                boolean flag2 = false;
                LineaActual += "for ( ";
                String iterador = ListaTokens.get(tokenActual + 1).getDato();
                tokenActual++;
                while (!ListaTokens.get(tokenActual).getIdToken().equals("fl")) {
                    String opcion = ListaTokens.get(tokenActual).getDato();
                    if (ListaTokens.get(tokenActual + 1).getIdToken().equals("(") && (opcion.equalsIgnoreCase("piso") || opcion.equalsIgnoreCase("techo") || opcion.equalsIgnoreCase("length") || opcion.equalsIgnoreCase("leer"))) {
                        LineaActual += Traducirmetodos(ListaTokens, LineasReales);
                    }

                    else if (ListaTokens.get(tokenActual).getDato().equals("<")) {
                        LineaActual += "; " + iterador + " < ";
                    }
                    else if (ListaTokens.get(tokenActual).getDato().equals(">")) {
                        LineaActual += "; " + iterador + " > ";
                        flag = true;
                    }
                    else if (ListaTokens.get(tokenActual).getDato().equals("{")) {
                        flag2 = true;
                    }
                    else {
                        LineaActual += ListaTokens.get(tokenActual).getDato();
                    }
                    tokenActual++;
                }
                if (flag && flag2) {
                    LineaActual += ";" + iterador + "-- ){";
                }
                else if (flag && !flag2) {
                    LineaActual += ";" + iterador + "-- )";
                }
                else if (!flag && flag2) {
                    LineaActual += ";" + iterador + "++ ){";
                }
                else {
                    LineaActual += ";" + iterador + "++ )";
                }
                Lineas.add(LineaActual);
                LineaAux = "script.stop(" + '"' + LineasReales.get(ListaTokens.get(tokenActual).getLinea()) + '"' + ", " + ListaTokens.get(tokenActual).getLinea() + ");";
                if (flag2) {
                    Lineas.add(LineaAux);
                    LineaAux = "";
                }
            }
            if (ListaTokens.get(tokenActual).getIdToken().equals("while") || ListaTokens.get(tokenActual).getIdToken().equals("if") || ListaTokens.get(tokenActual).getIdToken().equals("else")) {
                boolean flag = false;
                LineaActual = "";
                while (!ListaTokens.get(tokenActual).getDato().equalsIgnoreCase("fl")) {
                    String opcion = ListaTokens.get(tokenActual).getDato();
                    if (ListaTokens.get(tokenActual + 1).getIdToken().equals("(") && (opcion.equalsIgnoreCase("piso") || opcion.equalsIgnoreCase("techo") || opcion.equalsIgnoreCase("length") || opcion.equalsIgnoreCase("leer"))) {
                        LineaActual += Traducirmetodos(ListaTokens, LineasReales);
                    }
                    else if (ListaTokens.get(tokenActual).getDato().equalsIgnoreCase("{")) {
                        flag = true;
                    }
                    LineaActual += ListaTokens.get(tokenActual).getDato();
                    tokenActual++;
                }
                Lineas.add(LineaActual);
                LineaAux = "script.stop(" + '"' + LineasReales.get(ListaTokens.get(tokenActual).getLinea()) + '"' + ", " + ListaTokens.get(tokenActual).getLinea() + ");";
                if (flag) {
                    Lineas.add(LineaAux);
                    LineaAux = "";
                }
            }
            if (ListaTokens.get(tokenActual).getDato().equals("do{")) {
                Lineas.add("do{");
                LineaAux = "script.stop(" + '"' + LineasReales.get(ListaTokens.get(tokenActual).getLinea()) + '"' + ", " + ListaTokens.get(tokenActual).getLinea() + ");";
                Lineas.add(LineaAux);
                tokenActual++;
            }

            if (ListaTokens.get(tokenActual).getDato().equals("until")) {
                LineaActual = "}while ";
                tokenActual++;
                while (!ListaTokens.get(tokenActual).getDato().equalsIgnoreCase("fl")) {
                    LineaActual += ListaTokens.get(tokenActual).getDato();
                    tokenActual++;
                }
                Lineas.add(LineaActual + ";");
                LineaAux = "script.stop(" + '"' + LineasReales.get(ListaTokens.get(tokenActual).getLinea()) + '"' + ", " + ListaTokens.get(tokenActual).getLinea() + ");";
                Lineas.add(LineaAux);
            }

            //LLAMADO DE METODOS EN UNA LINEA
            if (ListaTokens.get(tokenActual).getIdToken().equals("id") && ListaTokens.get(tokenActual + 1).getDato().equals("(")) {
                LineaActual = "";
                if (ListaTokens.get(tokenActual).getDato().equals("imprimir") || ListaTokens.get(tokenActual).getDato().equals("add") || ListaTokens.get(tokenActual).getDato().equals("remove") || ListaTokens.get(tokenActual).getDato().equals("push") || ListaTokens.get(tokenActual).getDato().equals("pop") || ListaTokens.get(tokenActual).getDato().equals("length")) {
                    LineaActual += Traducirmetodos(ListaTokens, LineasReales);
                }
                else {
                    Lineas.add("script.estado(" + '"' + "espera" + '"' + ");");
                    LineaActual += TraducirMetodos(ListaTokens);

                }
                while (!ListaTokens.get(tokenActual).getDato().equalsIgnoreCase("fl")) {
                    tokenActual++;
                }
                Lineas.add(LineaActual + ";" + "script.estado(" + '"' + "activo" + '"' + ");");
                LineaAux = "script.stop(" + '"' + LineasReales.get(ListaTokens.get(tokenActual).getLinea()) + '"' + ", " + ListaTokens.get(tokenActual).getLinea() + ");";
                Lineas.add(LineaAux);
            }
            if ((tipos.contains(ListaTokens.get(tokenActual).getDato()) && ListaTokens.get(tokenActual + 2).getDato().equals("=") && ListaTokens.get(tokenActual + 1).getDato().equals("id")) || (ListaTokens.get(tokenActual).getIdToken().equals("id") && ListaTokens.get(tokenActual + 1).getDato().equals("=")) || (ListaTokens.get(tokenActual).getIdToken().equals("id") && ListaTokens.get(tokenActual + 1).getDato().equals("["))) {

                LineaActual = ListaTokens.get(tokenActual).getDato();
                System.out.println("ENTRE MADAFAKA" + ListaTokens.get(tokenActual + 1).getDato());
                tokenActual++;

                System.out.println("ENTRE MADAFAKA");
                while (!ListaTokens.get(tokenActual).getIdToken().equals("fl")) {
                    String opcion = ListaTokens.get(tokenActual).getDato();
                    //ASIGNAR A UNA VARIABLE EL VALOR RETORNADO POR UN LLAMADO A UN METODO
                    if (ListaTokens.get(tokenActual + 1).getIdToken().equals("(") && (opcion.equalsIgnoreCase("piso") || opcion.equalsIgnoreCase("techo") || opcion.equalsIgnoreCase("length") || opcion.equalsIgnoreCase("leer"))) {
                        LineaActual += Traducirmetodos(ListaTokens, LineasReales);
                    }
                    else if (ListaTokens.get(tokenActual + 1).getDato().equals("(") && !ListaTokens.get(tokenActual).getDato().equals("=")) {
                        Lineas.add("script.estado(" + '"' + "espera" + '"' + ");");
                        LineaActual += " " + TraducirMetodos(ListaTokens);

                    }
                    else {
                        LineaActual += " " + ListaTokens.get(tokenActual).getDato();
                    }
                    tokenActual++;
                }
                System.out.println("Aca coloque " + LineaAux);
                LineaAux = "script.stop(" + '"' + LineasReales.get(ListaTokens.get(tokenActual).getLinea()) + '"' + ", " + ListaTokens.get(tokenActual).getLinea() + ");";
                Lineas.add(LineaAux);
                Lineas.add(LineaActual + ";" + "script.estado(" + '"' + "activo" + '"' + ");");
                LineaAux = "";

            }

            tokenActual++;
        }

        String codigo = "";
        for (int i = 0; i < Lineas.size(); i++) {
            codigo += Lineas.get(i) + "\n";
            if (i == Lineas.size() - 2 && !retorno) {
                codigo += "script.estado(" + '"' + "inactivo" + '"' + ");";
            }
            System.out.println(Lineas.get(i));
        }

        return codigo;
    }

    /*
		Se encarga de traducir los métodos especiales y que el sistema
		reconoce como propios, en formato y estructura javascript
		Paramentros de entrada: Lista de tokens
	*/
	public String Traducirmetodos(LinkedList<Token> ListaTokens, TreeMap<Integer, String> LineasReales) {
        String linea = "";

        if (ListaTokens.get(tokenActual).getDato().equals("imprimir")) {

            linea += "script.imprimir( ";
            tokenActual += 2;
            linea += " " + ListaTokens.get(tokenActual).getDato();
            tokenActual++;
            linea += ")";
        }
        if (ListaTokens.get(tokenActual).getDato().equals("piso") && (ListaTokens.get(tokenActual + 1).getDato().equals("("))) {
            linea += "Math.floor(" + ListaTokens.get(tokenActual + 2).getDato() + ")";
            tokenActual += 3;
        }
        if (ListaTokens.get(tokenActual).getDato().equals("techo") && (ListaTokens.get(tokenActual + 1).getDato().equals("("))) {

            linea += "Math.ceil(" + ListaTokens.get(tokenActual + 2).getDato() + ")";
            tokenActual += 3;
        }
        if (ListaTokens.get(tokenActual).getDato().equals("leer") && (ListaTokens.get(tokenActual + 1).getDato().equals("("))) {
            linea += "script.leer()";
            tokenActual += 3;
        }
        if (ListaTokens.get(tokenActual).getDato().equals("length") && (ListaTokens.get(tokenActual + 1).getDato().equals("("))) {

            linea += ListaTokens.get(tokenActual + 2).getDato() + ".size()";
            tokenActual += 3;
        }

        if (ListaTokens.get(tokenActual).getDato().equals("push")) {
            linea += ListaTokens.get(tokenActual + 2).getDato() + ".push(" + ListaTokens.get(tokenActual + 4).getDato() + ");";
            tokenActual = tokenActual + 3;
        }

        if (ListaTokens.get(tokenActual).getDato().equals("pop")) {
            linea += ListaTokens.get(tokenActual + 2).getDato() + ".pop();";
            tokenActual = tokenActual + 2;
        }

        if (ListaTokens.get(tokenActual).getDato().equals("add")) {
            linea += ListaTokens.get(tokenActual + 2).getDato() + ".add(" + ListaTokens.get(tokenActual + 4).getDato() + ");";
            tokenActual = tokenActual + 3;
        }

        if (ListaTokens.get(tokenActual).getDato().equals("remove")) {
            linea += ListaTokens.get(tokenActual + 2).getDato() + ".remove(" + ListaTokens.get(tokenActual + 4).getDato() + ");";
            tokenActual = tokenActual + 3;
        }

        return linea;
    }

	/*
		Configura y detecta la declaracion de las variables y 
		estructuras que el usuario implemento en su algoritmo,
		creado y asignando su correspondiente valor y tipo en 
		formato javascript
		Paramentros de entrada: Lista de tokens
	*/
    public void configureScope(LinkedList<Token> ListaTokens, Script script) {

        for (int i = 0; i < ListaTokens.size(); i++) {
            if (ListaTokens.get(i).getIdToken().equals("int") && !ListaTokens.get(i - 1).getIdToken().equals("ES") && !ListaTokens.get(i - 1).getIdToken().equals("E")) {
                if (ListaTokens.get(i + 1).getIdToken().equals("id")) {
                    String id = ListaTokens.get(i + 1).getDato();
                    script.addVariable("int", id);
                    script.addToEscope(id, 0);
                }
            }
            if (ListaTokens.get(i).getIdToken().equals("double") && !ListaTokens.get(i - 1).getIdToken().equals("ES") && !ListaTokens.get(i - 1).getIdToken().equals("E")) {
                if (ListaTokens.get(i + 1).getIdToken().equals("id")) {
                    String id = ListaTokens.get(i + 1).getDato();
                    script.addVariable("double", id);
                    script.addToEscope(id, 0);
                }
            }
            if (ListaTokens.get(i).getDato().equals("string") && !ListaTokens.get(i - 1).getIdToken().equals("ES") && !ListaTokens.get(i - 1).getIdToken().equals("E")) {
                if (ListaTokens.get(i + 1).getIdToken().equals("id")) {
                    String id = ListaTokens.get(i + 1).getDato();
                    script.addVariable("string", id);
                    script.addToEscope(id, "");
                }
            }
            if (ListaTokens.get(i).getDato().equals("boolean") && !ListaTokens.get(i - 1).getIdToken().equals("ES") && !ListaTokens.get(i - 1).getIdToken().equals("E")) {
                if (ListaTokens.get(i + 1).getIdToken().equals("id")) {
                    String id = ListaTokens.get(i + 1).getDato();
                    //Dato nuevo = new Dato("boolean", id);
                    //Variables.add(nuevo);
                    script.addVariable("boolean", id);
                    script.addToEscope(id, false);
                }
            }
            if (ListaTokens.get(i).getIdToken().equals("int") && !ListaTokens.get(i - 1).getIdToken().equals("ES") && !ListaTokens.get(i - 1).getIdToken().equals("E")) {
                if (ListaTokens.get(i + 1).getDato().equals("[")) {
                    int j = i + 2;
                    while (!ListaTokens.get(j).getDato().equals("]")) {
                        j++;
                    }
                    String id = ListaTokens.get(j + 1).getDato();
                    script.addVariable("Array int", id);
                    script.addToEscope(id, new LinkedList<Integer>());
                }
            }
            if (ListaTokens.get(i).getDato().equals("boolean") && !ListaTokens.get(i - 1).getIdToken().equals("ES") && !ListaTokens.get(i - 1).getIdToken().equals("E")) {
                if (ListaTokens.get(i + 1).getIdToken().equals("[")) {
                    int j = i + 2;
                    while (!ListaTokens.get(j).getDato().equals("]")) {
                        j++;
                    }
                    String id = ListaTokens.get(j + 1).getDato();
                    script.addVariable("Array boolean", id);
                    script.addToEscope(id, new LinkedList<Boolean>());
                }
            }
            if (ListaTokens.get(i).getDato().equals("string") && !ListaTokens.get(i - 1).getIdToken().equals("ES") && !ListaTokens.get(i - 1).getIdToken().equals("E")) {
                if (ListaTokens.get(i + 1).getIdToken().equals("[")) {
                    int j = i + 2;
                    while (!ListaTokens.get(j).getDato().equals("]")) {
                        j++;
                    }
                    String id = ListaTokens.get(j + 1).getDato();
                    Dato nuevo = new Dato("Array String", id);
                    script.addVariable("Array String", id);
                    script.addToEscope(id, new LinkedList<String>());
                }
            }
            if (ListaTokens.get(i).getDato().equals("double") && !ListaTokens.get(i - 1).getIdToken().equals("ES") && !ListaTokens.get(i - 1).getIdToken().equals("E")) {
                if (ListaTokens.get(i + 1).getIdToken().equals("[")) {
                    int j = i + 2;
                    while (!ListaTokens.get(j).getDato().equals("]")) {
                        j++;
                    }
                    String id = ListaTokens.get(j + 1).getDato();
                    script.addVariable("Array double", id);
                    script.addToEscope(id, new LinkedList<Double>());

                }
            }
            //LISTAS, PILAS Y COLAS
            if (ListaTokens.get(i).getIdToken().equals("lista") && !ListaTokens.get(i - 1).getIdToken().equals("ES") && !ListaTokens.get(i - 1).getIdToken().equals("E")) {
                String id = ListaTokens.get(i + 2).getDato();

                if (ListaTokens.get(i + 1).getIdToken().equals("int")) {
                    GenerarLista(id, "listaInt", script);
                }
                else if (ListaTokens.get(i + 1).getIdToken().equals("double")) {
                    GenerarLista(id, "listaDouble", script);
                }
                else if (ListaTokens.get(i + 1).getIdToken().equals("string")) {
                    GenerarLista(id, "listaString", script);
                }
                else if (ListaTokens.get(i + 1).getIdToken().equals("boolean")) {
                    GenerarLista(id, "listaBoolean", script);
                }

                i = i + 2;
            }

            if (ListaTokens.get(i).getIdToken().equals("pila")) {
                //System.out.println("-------------------PILA-------------------------");
                String id = ListaTokens.get(i + 2).getDato();
                if (ListaTokens.get(i + 1).getIdToken().equals("int")) {
                    GenerarLista(id, "pilaInt", script);
                }
                else if (ListaTokens.get(i + 1).getIdToken().equals("double")) {
                    GenerarLista(id, "pilaDouble", script);
                }
                else if (ListaTokens.get(i + 1).getIdToken().equals("string")) {
                    GenerarLista(id, "pilaString", script);
                }
                else if (ListaTokens.get(i + 1).getIdToken().equals("boolean")) {
                    GenerarLista(id, "pilaBoolean", script);
                }

                i = i + 2;
            }
            if (ListaTokens.get(i).getIdToken().equals("cola")) {
                String id = ListaTokens.get(i + 2).getDato();
                if (ListaTokens.get(i + 1).getIdToken().equals("int")) {
                    GenerarLista(id, "colaInt", script);
                }
                else if (ListaTokens.get(i + 1).getIdToken().equals("double")) {
                    GenerarLista(id, "colaDouble", script);
                }
                else if (ListaTokens.get(i + 1).getIdToken().equals("string")) {
                    GenerarLista(id, "colaString", script);
                }
                else if (ListaTokens.get(i + 1).getIdToken().equals("boolean")) {
                    GenerarLista(id, "colaBoolean", script);
                }

                i = i + 2;
            }
        }

        JOptionPane in = new JOptionPane();
        // String a = in.showInputDialog(this, "");
        script.addToEscope("script", script);
        // script.addToEscope("inp", in);
    }

	/*
		Se encarga de configurar y asignar en codigo javascript
		los tipos de datos de estructuras como listas, pilas y colas
		Paramentros de entrada: identificador, tipo y el script que se 
		está creando
	*/
    public void GenerarLista(String id, String tipo, Script script) {
        script.addVariable(tipo, id);
        if (tipo.equals("listaInt") || tipo.equals("pilaInt") || tipo.equals("colaInt")) {
            script.addToEscope(id, new LinkedList<Integer>());
        }
        else if (tipo.equals("listaDouble") || tipo.equals("pilaDouble") || tipo.equals("colaDouble")) {
            script.addToEscope(id, new LinkedList<Double>());
        }
        else if (tipo.equals("listaString") || tipo.equals("pilaString") || tipo.equals("colaString")) {
            script.addToEscope(id, new LinkedList<String>());
        }
        else if (tipo.equals("listaBoolean") || tipo.equals("pilaBoolean") || tipo.equals("colaBoolean")) {
            script.addToEscope(id, new LinkedList<Boolean>());
        }

    }

    public Editor getInterfaz() {
        return Interfaz;
    }
/*
	Se encarga de eliminar del codigo generado en javascript que contengan
	patrones especificos para permitir la ejecucion corrida
	Paramentros de entrada: Cadena que representa al script a tratar
*/
    public String convertir(String cadena) {
        cadena = cadena.replaceAll("(script.stop\\(\\'.*\\'\\)\\;)", "");
        String aux[] = cadena.split("\n");
        String c = "";
        for (int i = 0; i < aux.length; i++) {
            if (!aux[i].equals("")) {
                c += aux[i] + "\n";
            }

        }
        return c;
    }

    public void setInterfaz(Editor Interfaz) {
        this.Interfaz = Interfaz;
    }

    public String limpieza() {
        String scripts = "";
        for (int i = 0; i < ListFunction.size(); i++) {
            scripts += convertir(ListFunction.get(i).getCode()) + "\n";
        }
        return scripts;
    }
}
