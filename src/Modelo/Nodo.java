/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.Serializable;
import java.util.*;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Anderlink234
 */
public class Nodo extends JLabel implements MouseListener, MouseMotionListener, Serializable {

    // variables necesarias para graficos
    private JPanel paneltem;
    /**
     * Posicion de imagen
     */
    private Point posicion = new Point(0, 0);
    /**
     * Tamaño de imagen
     */
    private Dimension d = new Dimension(31, 31);
    /**
     * variable que sirve para calcular el movimiento del objeto
     */
    private Point start_loc;
    /**
     * variable que sirve para calcular el movimiento del objeto
     */
    private Point start_drag;
    /**
     * variable que sirve para calcular el movimiento del objeto
     */
    private Point offset;
    /**
     * variables auxiliares para el desplazamiento del objeto
     */
    private int nuevo_X = 1;
    private int nuevo_Y = 1;
    //fin variables graficos
    private String Nombre;// a(entero) nombre
    private String Dato;//valor numerico
    private LinkedList<Nodo> Hijos;

    public Nodo(String Nombre, String dato, JPanel panel) {
        this.Nombre = Nombre;
        this.setToolTipText(Nombre);
        this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        this.setSize(d);
        this.setPreferredSize(d);
        this.Dato = dato;
        this.setVisible(true);
        this.setLocation(posicion);
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        this.paneltem = panel;
    }

    public String getDato() {
        return Dato;
    }

    public void setDato(String Dato) {
        this.Dato = Dato;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public LinkedList<Nodo> getHijos() {
        return Hijos;
    }

    public void setHijos(LinkedList<Nodo> Hijos) {
        this.Hijos = Hijos;
    }

    public JPanel getPaneltem() {
        return paneltem;
    }

    public void setPaneltem(JPanel paneltem) {
        this.paneltem = paneltem;
    }

    public Point getPosicion() {
        return posicion;
    }

    public void setPosicion(Point posicion) {
        this.posicion = posicion;
    }

    public Dimension getD() {
        return d;
    }

    public void setD(Dimension d) {
        this.d = d;
    }

    public Point getStart_loc() {
        return start_loc;
    }

    public void setStart_loc(Point start_loc) {
        this.start_loc = start_loc;
    }

    public Point getStart_drag() {
        return start_drag;
    }

    public void setStart_drag(Point start_drag) {
        this.start_drag = start_drag;
    }

    public Point getOffset() {
        return offset;
    }

    public void setOffset(Point offset) {
        this.offset = offset;
    }

    public int getNuevo_X() {
        return nuevo_X;
    }

    public void setNuevo_X(int nuevo_X) {
        this.nuevo_X = nuevo_X;
    }

    public int getNuevo_Y() {
        return nuevo_Y;
    }

    public void setNuevo_Y(int nuevo_Y) {
        this.nuevo_Y = nuevo_Y;
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        this.start_drag = getScreenLocation(e);
        this.start_loc = this.getLocation();
        this.paneltem.repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        nuevo_X = (this.getLocation().x);
        nuevo_Y = (this.getLocation().y);
        this.setLocation(nuevo_X, nuevo_Y);
        this.paneltem.repaint();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // this.setBorder(BorderFactory.createLineBorder(new java.awt.Color(204, 0, 51), 1));
    }

    @Override
    public void mouseExited(MouseEvent e) {
        this.setBorder(null);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        Point current = this.getScreenLocation(e);
        offset = new Point((int) current.getX() - (int) start_drag.getX(), (int) current.getY() - (int) start_drag.getY());
        Point new_location = new Point((int) (this.start_loc.getX() + offset.getX()), (int) (this.start_loc.getY() + offset.getY()));
        this.setLocation(new_location);
        this.paneltem.repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    private Point getScreenLocation(MouseEvent evt) {
        Point cursor = evt.getPoint();
        Point target_location = this.getLocationOnScreen();
        return new Point((int) (target_location.getX() + cursor.getX()),
                (int) (target_location.getY() + cursor.getY()));
    }
}
