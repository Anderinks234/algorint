/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingDeque;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 *
 * @author Anderlink234
 */
/*
Clase encargada de procesar graficamente la informacion del arbol de ejecucion y graficarlo
*/
public class PanelArbol extends javax.swing.JPanel implements Serializable {

    /**
     * Creates new form PanelArbol
     */
    Arbol derivacion;
    LinkedList<Nodo> listaNodos;
    LinkedList<LinkedList<Point>> lineas;
    boolean ok = false;
    Arbol derivaconCopia;
    ArbolEjecucion Arbol;
    LinkedList<NodoE> listaNodosE;

    @Override
    public void paint(Graphics g) {

        super.paint(g); //To change body of generated methods, choose Tools | Templates.
        ((Graphics2D) g).setStroke(new BasicStroke(2f));
        g.setColor(Color.black);

        if (derivacion != null) {

            if (!ok) {
                this.derivacion.setRaiz(derivaconCopia.getRaiz());
                repaint();
            }
            else {
                derivacion.getRaiz().setLocation(650, 0);
            }

            listaNodos.removeAll(listaNodos);
            lineas.removeAll(lineas);
            lineas = new LinkedList<>();
            listaNodos.add(this.derivaconCopia.getRaiz());

            verHijosRecursivo(this.derivacion.getRaiz());
            for (int i = 0; i < lineas.size(); i++) {
                g.setColor(Color.BLACK);
                g.drawLine(lineas.get(i).get(0).x + 15, lineas.get(i).get(0).y + 15, lineas.get(i).get(1).x + 15, lineas.get(i).get(1).y + 15);
            }
            for (int i = 0; i < listaNodos.size(); i++) {
                g.setColor(Color.blue);
                g.fillOval(listaNodos.get(i).getLocation().x, listaNodos.get(i).getLocation().y, 31, 31);
                g.setColor(Color.black);
                g.drawOval(listaNodos.get(i).getLocation().x, listaNodos.get(i).getLocation().y, 31, 31);
                g.setColor(Color.WHITE);

                g.drawString(listaNodos.get(i).getDato(), listaNodos.get(i).getLocation().x + 15, listaNodos.get(i).getLocation().y + 15);
            }
        }
        if (Arbol != null) {
            if (ok) {
                Acomodar(Arbol.getRaiz());
                ok = false;
            }
            g.setColor(Color.black);

            g.setColor(Color.white);
            if (Arbol.getRaiz() != null) {
                listaNodos.removeAll(listaNodos);
                lineas.removeAll(lineas);
                lineas = new LinkedList<>();
                listaNodosE.add(this.Arbol.getRaiz());

                verHijosRecursivo(this.Arbol.getRaiz());
                for (int i = 0; i < lineas.size(); i++) {
                    g.setColor(Color.BLACK);
                    g.drawLine(lineas.get(i).get(0).x + 15, lineas.get(i).get(0).y + 15, lineas.get(i).get(1).x + 15, lineas.get(i).get(1).y + 15);
                }
                for (int i = 0; i < listaNodosE.size(); i++) {
                    //System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"+listaNodosE.get(i).getDato().getEstado());
                    Color Estado = Color.decode("#ee8e7f");

                    if (listaNodosE.get(i).getDato().getEstado().equalsIgnoreCase("espera")) {
                        Estado = Color.decode("#fecf81");
                    }
                    else if (listaNodosE.get(i).getDato().getEstado().equalsIgnoreCase("inactivo")) {
                        Estado = Color.decode("#bab5c0");
                    }
                    else {
                        Estado = Color.decode("#a9f6be");
                    }
                    int yy = listaNodosE.get(i).getDato().getVariables().size() * 20;
                    if (yy < 100) {
                        yy = 100;
                    }
                    g.setColor(Estado);
                    g.fillRect(listaNodosE.get(i).getLocation().x, listaNodosE.get(i).getLocation().y, 120, yy);
                    g.setColor(Color.BLACK);
                    g.drawRect(listaNodosE.get(i).getLocation().x, listaNodosE.get(i).getLocation().y, 120, yy);
                    g.setColor(Color.BLACK);
                    int PosY = listaNodosE.get(i).getLocation().y + 12;
                    g.drawString(listaNodosE.get(i).getDato().getName() + " " + listaNodosE.get(i).getDato().getIdentificador() + " " + listaNodosE.get(i).getDato().getEstado(), listaNodosE.get(i).getLocation().x + 15, PosY);
                    PosY += 10;
                    g.drawLine(listaNodosE.get(i).getLocation().x, PosY, listaNodosE.get(i).getLocation().x + 120, PosY);
                    PosY += 10;
                    for (int j = 0; j < listaNodosE.get(i).getDato().getVariables().size(); j++) {
                        g.drawString(listaNodosE.get(i).getDato().getVariables().get(j).mostrarInfo(), listaNodosE.get(i).getLocation().x + 15, PosY);
                        PosY += 15;
                    }
                }
            }
        }
    }

    public void Acomodar(NodoE nodo) {
        int w = 1200;
        int x = w / 2;
        int y = 10;
        this.Arbol.getRaiz().setLocation(x, y);
        this.add(Arbol.getRaiz());
        int hijos = Arbol.getRaiz().getHijos().size();
        if (hijos > 0) {
            int fragmento = w / hijos;
            int partes = fragmento / 2;
            for (int i = 0; i < hijos; i++) {
                graficarR(Arbol.getRaiz().getHijos().get(i), fragmento * (i + 1), y + 50, x, y, partes, fragmento, fragmento * i);
            }
        }

    }

    public void graficarR(NodoE aux, int w, int y, int xa, int ya, int partes, int fragmento, int salva) {
        int x = w - partes;
        aux.setLocation(x, y);
        this.add(aux);
        int hijos = aux.getHijos().size();
        if (hijos > 0) {
            fragmento = fragmento / hijos;
            partes = fragmento / 2;
            for (int i = 0; i < hijos; i++) {
                graficarR((NodoE) aux.getHijos().get(i), fragmento * (i + 1) + salva, y + 50, x, y, partes, fragmento, fragmento * i);
            }
        }
    }

    public int Altura(NodoE nodo) {
        if (nodo.getHijos().size() == 0) {
            return 1;
        }
        int altura = 0;
        for (int i = 0; i < nodo.getHijos().size(); i++) {
            int aux = Altura(nodo.getHijos().get(i));
            if (aux > altura) {
                altura = aux;
            }
        }
        return altura + 1;
    }

    public void verHijosRecursivo(NodoE nodo) {
        if (nodo.getHijos() != null) {
            for (int i = 0; i < nodo.getHijos().size(); i++) {
                listaNodosE.add(nodo.getHijos().get(i));
                verHijosRecursivo(nodo.getHijos().get(i));
                LinkedList<Point> actual = new LinkedList<>();
                actual.add(new Point(nodo.getLocation()));
                actual.add(new Point(nodo.getHijos().get(i).getLocation()));
                lineas.add(actual);
            }
        }
    }

    public void verHijosRecursivo(Nodo nodo) {
        if (nodo.getHijos() != null) {
            for (int i = 0; i < nodo.getHijos().size(); i++) {
                listaNodos.add(nodo.getHijos().get(i));
                verHijosRecursivo(nodo.getHijos().get(i));
                LinkedList<Point> actual = new LinkedList<>();
                actual.add(new Point(nodo.getLocation()));
                actual.add(new Point(nodo.getHijos().get(i).getLocation()));
                lineas.add(actual);
            }
        }
    }

    public LinkedList<Nodo> getListaNodos() {
        return listaNodos;
    }

    public void setListaNodos(LinkedList<Nodo> listaNodos) {
        this.listaNodos = listaNodos;
    }

    public LinkedList<LinkedList<Point>> getLineas() {
        return lineas;
    }

    public void setLineas(LinkedList<LinkedList<Point>> lineas) {
        this.lineas = lineas;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public Arbol getDerivaconCopia() {
        return derivaconCopia;
    }

    public void setDerivaconCopia(Arbol derivaconCopia) {
        this.derivaconCopia = derivaconCopia;
    }

    public JButton getjButton1() {
        return jButton1;
    }

    public void setjButton1(JButton jButton1) {
        this.jButton1 = jButton1;
    }

    public Arbol getDerivacion() {
        return derivacion;
    }

    public void setDerivacion(Arbol derivacion) {
        this.derivacion = derivacion;
    }

    public PanelArbol() {
        initComponents();
        listaNodos = new LinkedList<>();
        lineas = new LinkedList<>();
        listaNodosE = new LinkedList<>();
    }

    public ArbolEjecucion getArbolEjecucion() {
        return Arbol;
    }

    public void setArbolEjecucion(ArbolEjecucion ArbolEjecucion) {
        this.Arbol = ArbolEjecucion;
    }

    public void acomodar() {
        this.derivacion.getRaiz().setLocation(0, 0);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();

        jButton1.setText("Reiniciar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 282, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jButton1)
                .addGap(0, 277, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        ok = true;
        repaint();
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    // End of variables declaration//GEN-END:variables
}
