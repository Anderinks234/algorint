/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.awt.Point;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JPanel;

/**
 *
 * @author Anderlink234
 */
public class SRL {

    LinkedList<String> Terminals;
    String[][] Table;
    LinkedList<LinkedList<String>> Reduce;
    public LinkedList<String> Pila;
    LinkedList<LinkedList<String>> Acciones;
    public Token Error;
    public boolean hayError = false;
    LinkedList<String> IndicesNume;
    String mensajeError = "";
    int indice = 0;

    /*
    Clase encargada de el analisis sintactico y armada del arbol de derivacion 
    
     */
    
    /*
    Constructor parametrizado
    Parametros de entrada: Recibe la tabla de Shift and Reduces, Y una lista (reduce) que corresponde a cada  una de las producciones de la gramatica
    */
    public SRL(LinkedList<String> Terminals, String[][] Table, LinkedList<LinkedList<String>> Reduce) {
        this.Terminals = Terminals;
        this.Table = Table;
        this.Reduce = Reduce;
        this.Pila = new LinkedList<>();
        Acciones = new LinkedList<>();
        IndicesNume = new LinkedList<>();
    }

    public LinkedList<String> getTerminals() {
        return Terminals;
    }

    public void setTerminals(LinkedList<String> Terminals) {
        this.Terminals = Terminals;
    }

    public String[][] getTable() {
        return Table;
    }

    public void setTable(String[][] Table) {
        this.Table = Table;
    }

    public LinkedList<LinkedList<String>> getReduce() {
        return Reduce;
    }

    public void setReduce(LinkedList<LinkedList<String>> Reduce) {
        this.Reduce = Reduce;
    }

    public LinkedList<String> getPila() {
        return Pila;
    }

    public void setPila(LinkedList<String> Pila) {
        this.Pila = Pila;
    }

    public LinkedList<LinkedList<String>> getAcciones() {
        return Acciones;
    }

    public void setAcciones(LinkedList<LinkedList<String>> Acciones) {
        this.Acciones = Acciones;
    }

    public Token getError() {
        return Error;
    }

    public void setError(Token Error) {
        this.Error = Error;
    }

    public boolean isHayError() {
        return hayError;
    }

    public void setHayError(boolean hayError) {
        this.hayError = hayError;
    }

    public LinkedList<String> getIndicesNume() {
        return IndicesNume;
    }

    public void setIndicesNume(LinkedList<String> IndicesNume) {
        this.IndicesNume = IndicesNume;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    public int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }

    void sout(LinkedList<Token> lista) {
        lista.stream().forEach((i) -> {
            System.out.print(i.nombre + " ");
        });
    }

    void sout2(LinkedList<String> lista) {
        lista.stream().forEach((i) -> {
            System.out.print(i + " ");
        });
    }
    /*
    Ejecuta el algoritmo SRL (Lecura de tabla), procesando los tokens apilando con la accion (Shift), despailando con la accion (Reduce)
    Parametros de entrada: Lista de tokens 
    */
    public LinkedList<LinkedList<String>> Derivar(LinkedList<Token> input) {
        LinkedList<Token> Copiainput = new LinkedList<>();
        for (int i = 0; i < input.size(); i++) {
            Copiainput.add(input.get(i));
        }

        Error = null;
        hayError = false;
        this.Pila = new LinkedList<>();
        Acciones = new LinkedList<>();
        IndicesNume = new LinkedList<>();

        boolean Block = false;
        //Preparar pila para iteraciones
        this.Pila.add("$");
        this.Pila.addFirst("0");
        input.add(new Token("$", "", "", 3, 3));

        while (!Block) {
            //Muestro contenido de la pila Y como esta el input despudes de cada operacion
            String Action = "";
            try {
                Action = this.Table[Integer.parseInt(this.Pila.getFirst().trim())][this.Terminals.indexOf(input.getFirst().getNombre().trim())];
            }
            catch (Exception e) {
                mensajeError = "Estaba esperando 2";
                Error = input.getFirst();
                hayError = true;
                System.out.println("Error");
                break;
            }

            if (Action == "") {
                mensajeError = "Token Inesperado";
                indice = Copiainput.indexOf(input.getFirst());
                Error = input.getFirst();
                hayError = true;
                Block = true;
            }
            else {
                input = RunAction(Action, input);
                if (Action.equals("a cc")) {
                    System.out.println("Derivacion correcta");
                    return Acciones;
                }
                else if (input.getFirst().getNombre().equals("Error")) {
                    mensajeError = "Token Inesperado";
                    System.out.println("Error durante la ejecucion de un Reduce!");
                    Error = input.getFirst();
                    hayError = true;
                    return null;
                }
            }
        }

        Error = input.getFirst();
        hayError = true;
        return null;
    }
    /*
        Ayuda al algortimo derivar() en el proceso de analisis sintactico, ejecutando instrucciones de propias del alogoritmo SLR-1
        que apila o desapila una estructura para el procesamiento sintactico
        Paramentros de entrada: Una accion y la lista de tokens a procesar
    */
    private LinkedList<Token> RunAction(String Action, LinkedList<Token> input) {
        String[] AuxAction = Action.split(" ");

        if (AuxAction[0].equals("s")) {

            this.Pila.addFirst(input.getFirst().getNombre());

            this.Pila.addFirst(AuxAction[1]);
            IndicesNume.add(Pila.getFirst());
            input.removeFirst();
            LinkedList<String> Shif = new LinkedList<>();
            Shif.add("Shif");
            Acciones.add(Shif);
            return input;
        }
        else if (AuxAction[0].equals("r")) {
            LinkedList<String> ReduceActual = this.Reduce.get(Integer.parseInt(AuxAction[1]) - 1);
            Acciones.add(ReduceActual);
            //caso que deriva vacio!->
            if (ReduceActual.size() == 1) {
                this.Pila.addFirst(ReduceActual.getFirst());
                if (!"".equals(this.Table[Integer.parseInt(Pila.get(1))][this.Terminals.indexOf(this.Pila.getFirst())])) {
                    this.Pila.addFirst(this.Table[Integer.parseInt(Pila.get(1))][this.Terminals.indexOf(this.Pila.getFirst())]);
                    IndicesNume.add(Pila.getFirst());

                    return input;

                }
                else {
                    mensajeError = "Token Inesperado";
                    Error = input.getFirst();
                    hayError = true;
                    input.addFirst(new Token("Error", "", "", 0, 0));
                    return input;
                }
            }
            else {

                for (int i = ReduceActual.size() - 1; i > 0; i--) {
                    if (ReduceActual.get(i).trim().equals(Pila.getFirst().trim())) {
                        this.Pila.removeFirst();
                        continue;
                    }
                    else {
                        boolean find = false;
                        while (!find) {
                            Pattern Expresion = Pattern.compile("[0-9]*");
                            Matcher Resultado = Expresion.matcher(this.Pila.getFirst().trim());
                            //                        if(IndicesNume.contains(Pila.getFirst())){
                            if (Resultado.matches()) {
                                this.Pila.removeFirst();
                            } //                      }
                            else if (this.Pila.getFirst().trim().equals(ReduceActual.get(i).trim())) {
                                this.Pila.removeFirst();
                                find = true;
                            }
                            else {
                                mensajeError = "Estaba esperando " + ReduceActual.get(i);
                                input.add(new Token("Error", "", "", 0, 0));
                                find = true;
                            }
                        }
                    }
                    if (input.getFirst().equals("Error")) {
                        mensajeError = "Token Inesperado";
                        Error = input.getFirst();
                        hayError = true;
                        return input;
                    }

                }

            }
            this.Pila.addFirst(ReduceActual.getFirst());
            this.Pila.addFirst(this.Table[Integer.parseInt(Pila.get(1).trim())][this.Terminals.indexOf(this.Pila.getFirst().trim())]);
            IndicesNume.add(Pila.getFirst());
        }
        return input;
    }

    /*
        Segun las listas de acciones de shifts y reduces generadas en el algoritmo runaction() construye el arbol de derivación
        sintactica,teniendo en cuenta que Shift que agrega tokens al arbol de derivacion, y reduce que simplifica y construye etapa a etapa el arbol final de derivacion
         donde se refleja como el codigo fue interpretado por la gramatica de este software
    */
    public Arbol ArmarArbol(LinkedList<Token> input, JPanel panelA) {
        //Crear todos los elementos como subarboles
        // Arbol ArbolF=new Arbol(panelA);
        int division = 1300 / input.size();
        int contador = 0;
        LinkedList<Arbol> Entrada = new LinkedList<>();
        for (int i = 0; i < input.size(); i++) {
            Arbol nuevo = new Arbol(panelA);
            Nodo nuevox = new Nodo(input.get(i).getNombre(), input.get(i).getDato(), panelA);
            nuevox.setLocation(contador, 600);
            panelA.add(nuevox);
            nuevo.setRaiz(nuevox);
            Entrada.add(nuevo);
            contador = contador + division;
        }
        LinkedList<Arbol> Espera = new LinkedList<>();
        //Recorro las acciones para procesarlas  
        for (int i = 0; i < Acciones.size(); i++) {
            if (Acciones.get(i).getFirst().equals("Shif")) {
                Espera.add(Entrada.getFirst());
                Entrada.removeFirst();
            }
            else {
                Arbol nuevo = new Arbol(panelA);

                Nodo aux = new Nodo(Acciones.get(i).getFirst(), Acciones.get(i).getFirst(), panelA);

                LinkedList<Nodo> Hijos = new LinkedList<>();
                for (int j = Acciones.get(i).size() - 1; j >= 1; j--) {
                    if (Acciones.get(i).get(j).equals(Espera.getLast().getRaiz().getNombre())) {
                        Hijos.add(Espera.getLast().getRaiz());
                        Espera.removeLast();
                    }
                    else {
                        return null;
                    }
                }
                if (Acciones.get(i).size() == 1) {

                    Nodo nuevox = new Nodo("K", "K", panelA);
                    Hijos.add(nuevox);
                    nuevox.setLocation(Espera.getLast().getRaiz().getLocation().x + 10, Espera.getLast().getRaiz().getLocation().y + 30);
                    panelA.add(nuevox);

                    aux.setLocation(Espera.getLast().getRaiz().getLocation().x + 10, mayor(Hijos).y - 20);
                    panelA.add(aux);
                    aux.setHijos(Hijos);
                    nuevo.setRaiz(aux);

                }
                else {

                    aux.setLocation(promedio(Hijos).x, mayor(Hijos).y - 20);
                    panelA.add(aux);
                    aux.setHijos(Hijos);
                    nuevo.setRaiz(aux);
                }

                Espera.add(nuevo);
            }

        }

        return Espera.getFirst();
    }

    /*
        Obtiene el punto mayor de todos los nodos del arbol, con fines de la graficacion de la estructura
        Parametros de  
    */
    public Point mayor(LinkedList<Nodo> hijos) {

        Point A = hijos.getFirst().getLocation();
        for (int i = 0; i < hijos.size(); i++) {
            if (hijos.get(i).getLocation().y < A.y) {
                A = hijos.get(i).getLocation();
            }
        }
        return A;
    }
    /*
        Calcula el promedio de un cojunto de nodos, con fines de la graficacion de la estructura
    */
    public Point promedio(LinkedList<Nodo> hijos) {
        int promedio = 0;
        for (int i = 0; i < hijos.size(); i++) {
            promedio += hijos.get(i).getLocation().x;
        }
        int a = hijos.size();
        promedio = promedio / a;

        return new Point(promedio, 0);
    }

}
