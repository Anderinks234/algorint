/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Clob;
import java.util.LinkedList;
import javax.script.Bindings;
import javax.script.Invocable;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.JFrame;
import javax.swing.JTextPane;
import Vista.Editor;
import javax.swing.JOptionPane;
import javax.swing.text.BadLocationException;

/**
 *
 * @author Ander
 */
 
 /*
	Es la clase que empaqueta toda la informacion necesaria para ejecutar un pseudocodigo
 */ 
public class Script implements Cloneable, Runnable {

    Editor interfaz;
    public String name;
    public ScriptEngineManager manager;
    public ScriptEngine engine;
    public String code;
    public Bindings scope;
    public String Linea;
    public LinkedList<Dato> Variables;
    public Compiler comp;
    public String[] Parametros;
    public LinkedList<Token> Tokens;
    public GodClass God;
    int identificador;
    String Estado;
    LinkedList<Integer> puntos;
    int punto;
	/*
		Constructor parametrizado de la clase
		Parametros de entrada: codigo,nombre y un compliador
	*/
    public Script(String codigo, String name, Compiler comp) {
        punto=0;
        this.manager = new ScriptEngineManager();
        this.engine = manager.getEngineByName("JavaScript");
        this.code = codigo;
        this.scope = engine.getBindings(ScriptContext.ENGINE_SCOPE);
        this.Variables = new LinkedList<>();
        this.name = name;
        this.comp = comp;
        scope.put("Comp", this.comp);
        identificador = 0;
        Estado = "";

    }
	/*
		Construcctor por defecto de la clase
	*/
    public Script() {
        this.manager = new ScriptEngineManager();
        this.engine = manager.getEngineByName("JavaScript");
        this.scope = engine.getBindings(ScriptContext.ENGINE_SCOPE);
        this.Variables = new LinkedList<>();
        engine.put("Comp", this.comp);

    }

	/*
		Agregan una variable con su tipo e identificador al scope
		del script actual
		Parametros de entrada: tipo e identificador
	*/
    public boolean addVariable(String tipo, String identificador) {
        for (int i = 0; i < Variables.size(); i++) {
            if (Variables.get(i).getIdentificador().equals(identificador)) {
                return false;
            }
        }
        Variables.add(new Dato(tipo, identificador));
        return true;
    }

	/*
		Devuelve el tipo de dato que le corresponde a un identificador
		Parametros de entrada: identificador de la variable requerida
	*/
    public String retornarTipo(String id) {
        for (int i = 0; i < Variables.size(); i++) {
            if (Variables.get(i).getIdentificador().equals(id)) {
                return Variables.get(i).getTipoDato();
            }
        }
        return "";
    }

	/*
		Permite visualizar todas las variables con sus respectivos tipos y valores
		en forma de tabla para mejorar la experiencia del usuario y el software
	*/
    public String verScope() {

        String salida = "PROCEDIMIENTO " + this.name + "\n";
        if (Variables != null) {
            for (int i = 0; i < Variables.size(); i++) {
                salida += Variables.get(i).mostrarInfo() + "\n";
                System.out.println(Variables.get(i).mostrarInfo());
            }
        }
        return salida;
    }
	/*
		Agraga una variable directamente al ambiente propio del script
		para que lo reconozca como una variable propia y pueda ser tratada
		dentro de el
		Parametros de entrada: Identificador y valor
	*/
    public boolean addToEscope(String Identificador, Object value) {
        if (this.scope.containsKey(Identificador)) {
            return false;
        }
        this.engine.put(Identificador, value);
        this.scope = engine.getBindings(ScriptContext.ENGINE_SCOPE);
        this.update();
        return true;
    }

	/*
		Permite detectar y extraer los nombre de las variables que son enviadas
		desde un método a otro, para que pueda ser compartido o clonado el valor
		requerido segun sea el caso
	*/
    public String[] ObtenerVariablesMetodos(String cadena) {
        System.out.println("-------->" + cadena);
        String sub[] = cadena.split("(\\(|\\))");
        if (sub.length > 0 && !sub[0].isEmpty() && sub[0] != "function") {
            cadena = sub[1];
            String variablesMetodos[] = cadena.split("\\,");
            for (int i = 0; i < variablesMetodos.length; i++) {
                System.out.println(variablesMetodos[i]);
            }
            return variablesMetodos;
        }
        return new String[0];
    }
	/*
		Con fines de claridad y orden grafico se utiliza este metodo para
		detectar cual es el estado de un metodo activo, en espera o finalizado
		Paramentros de entrada: estado del script actual
	*/
    public void estado(String estado) {
        this.Estado = estado;
        interfaz.getPanelEjecucion().setArbolEjecucion(interfaz.getArbol());
        interfaz.getPanelEjecucion().repaint();
        if(estado.equalsIgnoreCase("inactivo")){
            interfaz.TFin = System.currentTimeMillis(); 
        }
    }

	/*
		Actualiza el valor de las variables que se han modificado durante la ejecucion
		del codigo
	*/	
    public void update() {
        for (int i = 0; i < Variables.size(); i++) {
            Variables.get(i).setDato(scope.get(Variables.get(i).getIdentificador()));
        }
    }
	/*
		Configura el llamado a un metodo que ha sido llamado con sus respectivas variables,
		las que debe compartir por parametro o por referencia segun sea el caso 
		Paramentros de entrada: hasta, parametros
	*/
    public Object ComplexRun(Script to, String parametros) throws ScriptException, NoSuchMethodException, CloneNotSupportedException {
        parametros.trim();
        this.verScope();
        String Param[] = parametros.split(",");
        /*Añadimos parametros al scope de la funcion que se va a ejecutar "To", ojo,
        solo teniendo en cuenta que los parametros por el mementos son variables dentro del codigo del "From"*/
        if (Param.length > 0 && !Param[0].isEmpty()) {
            for (int i = 0; i < Param.length; i++) { //Importante que si no son ambos vctores del mismo tamaño hay error e envio de parametros
                to.addToEscope(to.Parametros[i].trim(), this.scope.get(Param[i].trim()));
               to.addVariable(this.retornarTipo(Param[i].trim()), to.Parametros[i].trim());
                to.update();
            }

            System.out.println("Escope DESDE");
            System.out.println(verScope());
            System.out.println("Esocpe HASTA");
            System.out.println(to.verScope());

        }
        else {
            System.out.println("Salta, parametros vacios");
        }
        return to.SimpleRun();
    }

	/*
	Detecta si al llamarse un método debe compartir alguno de sus parametros
	*/
    public Object SimpleRun() throws ScriptException, NoSuchMethodException, CloneNotSupportedException {

        String codeTemporal = this.code;
        if (Parametros.length > 0) {
            for (String Parametro : Parametros) {
                codeTemporal = codeTemporal.replaceFirst(Parametro, " ");
            }
            for (int i = 0; i < Parametros.length - 1; i++) {
                if (!Parametros[i].isEmpty()) {
                    codeTemporal = codeTemporal.replaceFirst(",", "");
                }
            }
        }
        this.engine.eval(codeTemporal);
        Invocable inv = (Invocable) this.engine;
        
        Object x = inv.invokeFunction(this.name);
        this.update();
        String Info = "";
        Info += "Estado de las variables \n";
        Info += this.verScope();
        System.out.println(Info);
        interfaz.output.setText(Info);

        return x;
    }

    public Object leer() {
        return JOptionPane.showInputDialog("Escriba valor");
    }
	/*
	Imprime en un elemento grafico la  informacion enviada desde un procedimiento javascript
	Parametros de entrada: el dato a imprimir
	*/
    public void imprimir(Object dato) {
        JOptionPane.showMessageDialog(interfaz, dato, "Resultado", 0);
        interfaz.output.setText(interfaz.output.getText() + "\n" + dato);
    }
	/*
		Modifica el numero de veces que una linea que se ha ejecutado
		Parametros de entrada: linea
	*/
    public void ActualizarLineas(int linea) {
        if (interfaz.ContadorLineas.containsKey(linea)) {
            int numero = interfaz.ContadorLineas.get(linea);
            interfaz.ContadorLineas.remove(linea);
            interfaz.ContadorLineas.put(linea, numero + 1);
        }
        else {
            interfaz.ContadorLineas.put(linea, 1);
        }
    }
	/*
		Permite realizar una pausa en el la ejecucion de codigo, ya que se 
		encuentra en un subproceso aparte y de esta manera no bloque la interfaz,
		actualiza el estado de las variables, el arbol de ejecucion y resalta
		la linea actual que se esta ejecutando
		Parametros de entrada: Lina real y la linea
	*/
    public void stop(String LineaReal, int linea) throws InterruptedException, BadLocationException {
        ActualizarLineas(linea);
        this.update();
        String Info = "";
        Info = " Linea en Ejecucion:   " + LineaReal + "\n";
        Info += "Estado de las variables \n";
        Info += this.verScope();
        System.out.println(Info);
        interfaz.getLineP().setLinea(linea);
        interfaz.getLineP().setOk(true);
        interfaz.getLineP().resetHighlight();
        interfaz.getLineP().getComponent().repaint();
        interfaz.output.setText(Info);
        
        if (interfaz.isPasoPaso() || interfaz.BreakPoints.size() > 0) {
            if (interfaz.BreakPoints.contains(linea) || interfaz.isPasoPaso()) {
                
                interfaz.getPanelEjecucion().repaint();
                interfaz.getLineP().setLinea(linea);
                interfaz.getLineP().setOk(true);
                interfaz.getLineP().resetHighlight();
                interfaz.getLineP().getComponent().repaint();
                
                if (interfaz.BreakPoints.contains(linea))
                {
                punto++;
                JOptionPane.showMessageDialog(interfaz, "Break point en la linea "+linea+" \n Se ha ejecutado "+punto+" veces");
                }
                while (!interfaz.stop) {
                    System.out.print("");
                }
                interfaz.stop = false;
            }
        }
    }
	/*
		Proceso que actualiza todos lo relacionado con la ejecucion de codigo
		y lo muestra los resultados en la interfaz principal
	*/
    @Override
    public void run() {
        String Info = "Linea en Ejecucion:   " + Linea;
        Info += "Estado de las variables \n";
        this.update();
        Info += interfaz.output.getText() + this.verScope();
        interfaz.output.setText(interfaz.output.getText() + "\n" + Info);

        while (!interfaz.stop) {
            System.out.println("");
        }
        interfaz.stop = false;
    }

    public GodClass getGod() {
        return God;
    }

    public void setGod(GodClass God) {
        this.God = God;
    }

    public String getCodigo() {
        return code;
    }

    public void setCodigo(String codigo) {
        this.code = codigo;
    }

    public Bindings getScope() {
        return scope;
    }

    public void setScope(Bindings scope) {
        this.scope = scope;
    }

    public ScriptEngineManager getManager() {
        return manager;
    }

    public void setManager(ScriptEngineManager manager) {
        this.manager = manager;
    }

    public ScriptEngine getEngine() {
        return engine;
    }

    public void setEngine(ScriptEngine engine) {
        this.engine = engine;
    }

    public LinkedList<Dato> getVariables() {
        return Variables;
    }

    public void setVariables(LinkedList<Dato> Variables) {
        this.Variables = Variables;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
        this.Parametros = ObtenerVariablesMetodos(this.code);
    }

    public Compiler getComp() {
        return comp;
    }

    public void setComp(Compiler comp) {
        this.comp = comp;
        engine.put("Comp", this.comp);
        this.scope = engine.getBindings(ScriptContext.ENGINE_SCOPE);

    }

    public LinkedList<Token> getTokens() {
        return Tokens;
    }

    public void setTokens(LinkedList<Token> Tokens) {
        this.Tokens = Tokens;
    }

    public Editor getInterfaz() {
        return interfaz;
    }

    public void setInterfaz(Editor interfaz) {
        this.interfaz = interfaz;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

}
