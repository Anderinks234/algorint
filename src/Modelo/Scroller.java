/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.awt.*;
import java.awt.HeadlessException;
import javax.swing.*;
import javax.swing.JPanel;

/**
 *
 * @author Anderlink234
 */
public class Scroller extends JFrame {
    
    public Scroller(JPanel panel) throws HeadlessException {
        this.setTitle("ARBOL");
        panel.setBorder(BorderFactory.createLineBorder(Color.red));
        panel.setPreferredSize(new Dimension(800, 600));
        
        final JScrollPane scroll = new JScrollPane(panel,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
        JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setLayout(new BorderLayout());
        add(scroll, BorderLayout.CENTER);
        setSize(1366, 700);
        this.dispose();
       
    }

    public static void main(final String[] args) throws Exception {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
              new Scroller(new JPanel()).setVisible(true);
                
            }
        });
    }
}

