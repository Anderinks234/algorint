/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;

/**
 *
 * @author Anderlinks234
 */
public class Token implements Serializable{
    String nombre;// simbolo de representacion gramatica
    String dato;//
    String idToken;//palabras claves
    int linea;
    int columna;
    
    public Token(){
        this.nombre="";
        this.dato="";
        idToken="";
    }

    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String nombreToken) {
        this.idToken = nombreToken;
    }

    public int getLinea() {
        return linea;
    }

    public void setLinea(int linea) {
        this.linea = linea;
    }

    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }
    
    public Token(String nombre, String dato, String idToken, int linea,  int columna){
        this.nombre=nombre;
        this.dato=dato;
        this.idToken=idToken;
        this.linea=linea;
        this.columna=columna;
        
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDato() {
        return dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }
    
    
}
