/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;



/**
 *
 * @author Anderlink234
 */
public class Variable {
    String Tipo;
    String Nombre;
    String Valor;
    String Parametros ;

    public Variable(String Tipo, String Nombre, String Valor, String Parametros) {
        this.Tipo = Tipo;
        this.Nombre = Nombre;
        this.Valor = Valor;
        this.Parametros = Parametros;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getValor() {
        return Valor;
    }

    public void setValor(String Valor) {
        this.Valor = Valor;
    }

    public String getParametros() {
        return Parametros;
    }

    public void setParametros(String Parametros) {
        this.Parametros = Parametros;
    }
    
    


}
