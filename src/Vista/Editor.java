package Vista;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import iconos.*;
import java.awt.Color;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Element;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import Modelo.*;
//Revisar librerias que no se estan utilizando
//Para subir a git
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static javafx.scene.paint.Color.color;
import javax.script.ScriptException;
import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.SimpleAttributeSet;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter.DefaultHighlightPainter;
import javax.swing.text.Highlighter;
import javax.swing.text.JTextComponent;
import static javafx.scene.paint.Color.color;
import static javafx.scene.paint.Color.color;
import static javafx.scene.paint.Color.color;
import static javafx.scene.paint.Color.color;
import static javafx.scene.paint.Color.color;
import static javafx.scene.paint.Color.color;
import static javafx.scene.paint.Color.color;

public class Editor extends javax.swing.JFrame implements Runnable {

    public long TInicio, TFin, tiempo; //Variables para determinar el tiempo de ejecución
    public TreeMap<Integer, Integer> ContadorLineas;
    private boolean primera = false;
    private LinkedList<LinkedList<String>> Tablas;
    private Color color;
    private LinePainter LineP;
    private LinkedList<Boolean> Repetidas;
    private Rectangle lastView;
    private JTextPane txt;
    public static final Color DEFAULT_KEYWORD_COLOR = Color.red;
    private Analizador_Lexico AnalizadorLexico;
    private SRL AnalizadorSintactico;
    private LinkedList<LinkedList<Token>> FPExistentes;
    public int linea = 1;
    public boolean ok = true;
    private boolean primeraVez = true;
    private boolean pasoPaso = false;
    public JTextPane output;
    public boolean stop = false;
    private LinkedList<Token> Input3 = new LinkedList();
    private Thread Hilo = new Thread(this);
    private String Errores = "";
    private int indiceTabla = 0;
    private SimpleAttributeSet aSet = new SimpleAttributeSet();
    private String codigos = "";
    private PanelArbol PanelEjecucion;
    private ArbolEjecucion Arbol;
    private Scroller VentanaArbol;
    public LinkedList<Integer> BreakPoints;
    private JDialog MenuSimulacion = new JDialog();
    private TreeMap<String, TreeMap<Integer, Point>> Informacion;

    public Editor() {
        initComponents();
		
        jButton1.setMnemonic('z');
        Informacion = new TreeMap<>();
        ContadorLineas = new TreeMap<>();
        BreakPoints = new LinkedList<>();
        Repetidas = new LinkedList<>();
        Tablas = new LinkedList<>();
        CrearAnalizadorLexico();
        CrearAnalizadorSintactico();
        this.output = new JTextPane();
        this.output.setBounds(5, 500, 950, 120);
        JScrollPane outputScroll = new JScrollPane(this.output);
        outputScroll.setVisible(true);
        outputScroll.setBounds(50, 490, 900, 110);
        this.setBackground(Color.yellow);
        this.setSize(1000, 700);
        this.setResizable(false);
        this.setTitle("AlgorINT v1.0");
        PanelEjecucion = new PanelArbol();
        VentanaArbol = new Scroller(PanelEjecucion);
        Arbol = new ArbolEjecucion(PanelEjecucion);
        this.jPanel1.setBounds(0, 0, 1000, 50);
        this.jPanel1.setBackground(Color.DARK_GRAY);
        this.panel.setBounds(0, 50, 1000, 650);

        //aca creo lo que corresponde al editor de texto para pitnar las palabras
        final StyleContext cont = StyleContext.getDefaultStyleContext(); //estilo
        final AttributeSet atrRojo = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.RED); //atributo que pinta rojo
        final AttributeSet atrAzul = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.BLUE); //atributo que pinta azul
        final AttributeSet atrNegro = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.BLACK); //
        DefaultStyledDocument doc = new DefaultStyledDocument() {

            public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
                super.insertString(offset, str, a);

                String text = getText(0, getLength());
                int before = findLastNonWordChar(text, offset);
                if (before < 0) {
                    before = 0;
                }
                int after = findFirstNonWordChar(text, offset + str.length());
                int wordL = before;
                int wordR = before;

                while (wordR <= after) {
                    if (wordR == after || String.valueOf(text.charAt(wordR)).matches("\\W")) {
                        if (text.substring(wordL, wordR).matches("(\\W)*(begin|end|protected|procedure|function)")) {
                            setCharacterAttributes(wordL, wordR - wordL, atrRojo, false);
                        }
                        else if (text.substring(wordL, wordR).matches("(\\W)*(while|then|for|do|end|int|double|false|string|boolean|F|T|or|and|repeat|until)")) {
                            setCharacterAttributes(wordL, wordR - wordL, atrAzul, false);

                        }
                        else {
                            setCharacterAttributes(wordL, wordR - wordL, atrNegro, false);
                        }
                        wordL = wordR;
                    }
                    wordR++;
                }
            }

            public void remove(int offs, int len) throws BadLocationException {
                super.remove(offs, len);

                String text = getText(0, getLength());
                int before = findLastNonWordChar(text, offs);

                if (before < 0) {
                    before = 0;
                }
                int after = findFirstNonWordChar(text, offs);

                if (text.substring(before, after).matches("(\\W)*(private|public|protected)")) {
                    setCharacterAttributes(before, after - before, atrRojo, false);
                }
                else if (text.substring(before, after).matches("(\\W)*(begin|else|if)")) {
                    setCharacterAttributes(before, after - before, atrAzul, false);
                }
                else {
                    setCharacterAttributes(before, after - before, atrNegro, false);
                }
            }
        };
        txt = new JTextPane(doc);
        txt.setFont(new Font("OCR A Extended", Font.PLAIN, 14));
        //txt.setBounds(150, 55, 800, 540);
        JScrollPane scroll = new JScrollPane(txt);
        TextLineNumber tln = new TextLineNumber(txt);
        // posicionParado(); //este metodo devuelve la posicion de la linea en donde se esta parado
        txt.setBorder(BorderFactory.createLineBorder(Color.BLUE, 2));

        scroll.setBounds(5, 10, 950, 440);
        scroll.setRowHeaderView(tln);
        scroll.setVisible(true);

        this.panel.add(scroll);
        this.output.setVisible(true);
        output.setBorder(BorderFactory.createLineBorder(Color.BLUE, 2));

        output.setBackground(Color.BLACK);
        output.setForeground(Color.BLUE);
        this.panel.add(outputScroll);
        LineP = new LinePainter(this.txt);
        LineP.setBreakPoints(BreakPoints);

    }

    public void setArbol(ArbolEjecucion Arbol) {
        this.Arbol = Arbol;
    }

    public String[][] acomodar(String[][] m) {
        String[][] m2 = new String[m.length][m[0].length];
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                if (m[i][j].equals("")) {
                    System.out.print("__");
                    m2[i][j] = "";
                }
                else if (m[i][j].length() > 0) {
                    String a = m[i][j];
                    Pattern Expresion = Pattern.compile("[0-9]*");
                    Matcher Resultado = Expresion.matcher(a.trim());
                    if (Resultado.matches()) {
                        m2[i][j] = a;
                    }
                    else {
                        a = a.charAt(0) + " " + m[i][j].substring(1, m[i][j].length());
                        System.out.print(a);
                        m2[i][j] = a;
                    }
                }
            }
            System.out.println("");

        }
        return m2;
    }

    public String[][] leer() throws IOException, BiffException {

        Workbook w = Workbook.getWorkbook(new File("MatrizSRL.xls")); //Pasamos el excel que vamos a leer
        Sheet sheet = w.getSheet(0); //Seleccionamos la hoja que vamos a leer
        String nombre;
        String[][] m = new String[sheet.getRows()][sheet.getColumns()];
        for (int fila = 0; fila < sheet.getRows(); fila++) { //recorremos las filas
            for (int columna = 0; columna < sheet.getColumns(); columna++) { //recorremos las columnas
                nombre = sheet.getCell(columna, fila).getContents(); //setear la celda leida a nombre
                m[fila][columna] = nombre;
            }
        }
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                System.out.print(" " + m[i][j]);
            }
        }
        return m;
    }

    public LinkedList<String> llenar(String r) {
        LinkedList<String> produccion = new LinkedList<String>();
        String[] aux = r.split(" ");

        for (int i = 0; i < aux.length; i++) {
            produccion.add(aux[i]);
        }
        return produccion;
    }

    public void Compilar() {
        LinkedList<Token> Input = AnalizadorLexico.GenerarTonkens(this.txt.getText());
        Input = ModificarStirng(Input);
        Input3 = new LinkedList<>();
        LinkedList<Token> Input2 = new LinkedList<Token>();
        System.out.println("Nuevos");
        for (int i = 0; i < Input.size(); i++) {
            if (!Input.get(i).getIdToken().equals("fl")) {
                Input2.add(Input.get(i));
                Input3.add(Input.get(i));
                System.out.print(Input.get(i).getNombre());
            }
        }

        PartirFuncionesYProcedimientos(Input2);
        AnalizadorSintactico.Pila.removeAll(AnalizadorSintactico.Pila);
        AnalizadorSintactico.Error = null;
        AnalizadorSintactico.Derivar(Input2);

    }

    public LinkedList<Token> ModificarStirng(LinkedList<Token> Tokens) {

        for (int i = 0; i < Tokens.size(); i++) {

            if (Tokens.get(i).getNombre().equals("¢")) {
                int con = i + 1;
                String Dto = "";
                if (con <= Tokens.size()) {
                    while (!Tokens.get(con).getNombre().equals("¢")) {

                        //System.out.println("Elimine el token "+Tokens.get(con).getIdToken());
                        Dto += " " + Tokens.get(con).getDato();
                        Tokens.remove(con);
                        //System.out.println("..."+Dto);
                    }

                }

                Token nuevo = new Token("¤", Dto, "cadena", Tokens.get(i).getLinea(), Tokens.get(i).getColumna());
                Tokens.add(con, nuevo);
                Tokens.remove(con - 1);
                Tokens.remove(con);

            }

        }
        return Tokens;
    }

    public void GenerarProducciones() {
        LinkedList<String> dentroPar = new LinkedList<>();
        String contenido = "";
        String[] lineas = this.txt.getText().split("\n");
        for (int i = 0; i < lineas.length; i++) {
            contenido = "";
            for (int j = 0; j < lineas[i].length() - 1; j++) {
                if (j < lineas[i].length() - 1) {
                    contenido += "" + lineas[i].charAt(j) + " ";
                }
                else {
                    contenido += "" + lineas[i].charAt(j);
                }
            }
            //System.out.println(contenido);
            System.out.println("producciones.add(llenar('" + contenido.trim() + "'));");

        }

    }

    public void CrearAnalizadorLexico() {
        AnalizadorLexico = new Analizador_Lexico();
        AnalizadorLexico.AñadirToken("");

        //--------------------
        AnalizadorLexico.AñadirToken("");
        AnalizadorLexico.AñadirToken("a");
        AnalizadorLexico.AñadirToken("b");
        AnalizadorLexico.AñadirToken("c");
        AnalizadorLexico.AñadirToken("d");
        AnalizadorLexico.AñadirToken("e");
        AnalizadorLexico.AñadirToken("f");
        AnalizadorLexico.AñadirToken("g");
        AnalizadorLexico.AñadirToken("h");
        AnalizadorLexico.AñadirToken("i");
        AnalizadorLexico.AñadirToken("j");
        AnalizadorLexico.AñadirToken("k");
        AnalizadorLexico.AñadirToken("l");
        AnalizadorLexico.AñadirToken("m");
        AnalizadorLexico.AñadirToken("n");
        AnalizadorLexico.AñadirToken("ñ");
        AnalizadorLexico.AñadirToken("o");
        AnalizadorLexico.AñadirToken("p");
        AnalizadorLexico.AñadirToken("q");
        AnalizadorLexico.AñadirToken("l");
        AnalizadorLexico.AñadirToken("r");
        AnalizadorLexico.AñadirToken("s");
        AnalizadorLexico.AñadirToken("t");
        AnalizadorLexico.AñadirToken("u");
        AnalizadorLexico.AñadirToken("v");
        AnalizadorLexico.AñadirToken("w");
        AnalizadorLexico.AñadirToken("x");
        AnalizadorLexico.AñadirToken("y");
        AnalizadorLexico.AñadirToken("z");

        AnalizadorLexico.AñadirToken("á");
        AnalizadorLexico.AñadirToken("é");
        AnalizadorLexico.AñadirToken("í");
        AnalizadorLexico.AñadirToken("ó");
        AnalizadorLexico.AñadirToken("ú");
        AnalizadorLexico.AñadirToken("æ");
        AnalizadorLexico.AñadirToken("ç");
        AnalizadorLexico.AñadirToken("+");
        AnalizadorLexico.AñadirToken("-");
        AnalizadorLexico.AñadirToken("*");
        AnalizadorLexico.AñadirToken("/");
        AnalizadorLexico.AñadirToken("ć");
        AnalizadorLexico.AñadirToken("ű");
        AnalizadorLexico.AñadirToken("┌");
        AnalizadorLexico.AñadirToken("┐");
        AnalizadorLexico.AñadirToken("└");
        AnalizadorLexico.AñadirToken("┘");
        AnalizadorLexico.AñadirToken("z");
        AnalizadorLexico.AñadirToken("ŵ");
        AnalizadorLexico.AñadirToken("ɘ");
        AnalizadorLexico.AñadirToken("µ");
        AnalizadorLexico.AñadirToken("°");
        AnalizadorLexico.AñadirToken("¢");
        AnalizadorLexico.AñadirToken("!");
        AnalizadorLexico.AñadirToken("ĝ");
        AnalizadorLexico.AñadirToken("¤");
        AnalizadorLexico.AñadirToken("@");
        AnalizadorLexico.AñadirToken("ɷ");
        AnalizadorLexico.AñadirToken("?");
        AnalizadorLexico.AñadirToken(",");
        AnalizadorLexico.AñadirToken(".");
        AnalizadorLexico.AñadirToken("ɐ");
        AnalizadorLexico.AñadirToken("{");
        AnalizadorLexico.AñadirToken("}");
        AnalizadorLexico.AñadirToken("¡");
        AnalizadorLexico.AñadirToken("à");
        AnalizadorLexico.AñadirToken("ē");
        AnalizadorLexico.AñadirToken("ď");

        //--------------------
        AnalizadorLexico.AñadirEstado(false, "", "", "q0");
        AnalizadorLexico.AñadirEstado(true, "+", "+", "q1");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q2");
        AnalizadorLexico.AñadirEstado(true, "¢", "error", "q3");
        AnalizadorLexico.AñadirEstado(true, "*", "*", "q4");
        AnalizadorLexico.AñadirEstado(true, "/", "/", "q5");
        AnalizadorLexico.AñadirEstado(true, "á", "<", "q6");
        AnalizadorLexico.AñadirEstado(true, "é", ">", "q7");
        AnalizadorLexico.AñadirEstado(true, "í", "<=", "q8");
        AnalizadorLexico.AñadirEstado(true, "ó", ">=", "q9");
        AnalizadorLexico.AñadirEstado(true, "ú", "<>", "q10");

        AnalizadorLexico.AñadirEstado(true, "ç", "<-", "q11");
        AnalizadorLexico.AñadirEstado(true, "æ", "=", "q12");
        AnalizadorLexico.AñadirEstado(true, "!", "skip", "q13");
        AnalizadorLexico.AñadirEstado(true, "┌", "┌", "q14");
        AnalizadorLexico.AñadirEstado(true, "┐", "┐", "q15");
        AnalizadorLexico.AñadirEstado(true, "└", "└", "q16");
        AnalizadorLexico.AñadirEstado(true, "┘", "┘", "q17");
        // AnalizadorLexico.AñadirEstado(true, "ú", "q18");
        AnalizadorLexico.AñadirEstado(true, "o", "#", "q19");
        AnalizadorLexico.AñadirEstado(true, "r", "(", "q20");

        AnalizadorLexico.AñadirEstado(true, "s", ")", "q21");
        AnalizadorLexico.AñadirEstado(true, "p", "[", "q22");
        AnalizadorLexico.AñadirEstado(true, "q", "]", "q23");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q24");
        AnalizadorLexico.AñadirEstado(true, "a", "if", "q25");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q26");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q27");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q28");
        AnalizadorLexico.AñadirEstado(true, "b", "else", "q29");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q30");

        AnalizadorLexico.AñadirEstado(true, "d", "end", "q31");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q32");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q33");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q34");
        AnalizadorLexico.AñadirEstado(true, "c", "then", "q35");
        AnalizadorLexico.AñadirEstado(true, "j", "to", "q36");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q37");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q38");
        AnalizadorLexico.AñadirEstado(true, "f", "for", "q39");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q40");

        AnalizadorLexico.AñadirEstado(true, "i", "id", "q41");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q42");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q43");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q44");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q45");
        AnalizadorLexico.AñadirEstado(true, "n", "function", "q46");
        AnalizadorLexico.AñadirEstado(true, "y", "T", "q47");
        AnalizadorLexico.AñadirEstado(true, "z", "F", "q48");
        AnalizadorLexico.AñadirEstado(true, "ŵ", "E", "q49");
        AnalizadorLexico.AñadirEstado(true, "ɘ", "ES", "q50");

        AnalizadorLexico.AñadirEstado(true, "i", "id", "q51");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q52");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q53");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q54");
        AnalizadorLexico.AñadirEstado(true, "e", "while", "q55");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q56");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q57");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q58");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q59");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q60");

        AnalizadorLexico.AñadirEstado(true, "g", "repeat", "q61");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q62");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q63");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q64");
        AnalizadorLexico.AñadirEstado(true, "ñ", "return", "q65");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q66");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q67");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q68");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q69");
        AnalizadorLexico.AñadirEstado(true, "h", "until", "q70");

        AnalizadorLexico.AñadirEstado(true, "i", "id", "q71");
        AnalizadorLexico.AñadirEstado(true, "k", "do", "q72");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q73");
        AnalizadorLexico.AñadirEstado(true, "ű", "div", "q74");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q75");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q76");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q77");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q78");
        AnalizadorLexico.AñadirEstado(true, "l", "id", "q79");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q80");

        AnalizadorLexico.AñadirEstado(true, "i", "id", "q81");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q82");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q83");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q84");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q85");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q86");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q87");
        AnalizadorLexico.AñadirEstado(true, "m", "procedure", "q88");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q89");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q90");

        AnalizadorLexico.AñadirEstado(true, "i", "id", "q91");
        AnalizadorLexico.AñadirEstado(true, "t", "null", "q92");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q93");
        AnalizadorLexico.AñadirEstado(true, "x", "not", "q94");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q95");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q96");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q97");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q98");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q99");
        AnalizadorLexico.AñadirEstado(true, "u", "lenght", "q100");

        AnalizadorLexico.AñadirEstado(true, "i", "id", "q101");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q102");
        AnalizadorLexico.AñadirEstado(true, "v", "and", "q103");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q104");
        AnalizadorLexico.AñadirEstado(true, "w", "or", "q105");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q106");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q107");
        AnalizadorLexico.AñadirEstado(true, "ć", "mod", "q108");
        AnalizadorLexico.AñadirEstado(true, "µ", "entero", "q109");
        AnalizadorLexico.AñadirEstado(true, "ĝ", "decimal", "q110");

        AnalizadorLexico.AñadirEstado(true, "¢", "error", "q111");
        AnalizadorLexico.AñadirEstado(true, "¤", "cadena", "q112");
        AnalizadorLexico.AñadirEstado(true, "-", "-", "q113");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q114");
        AnalizadorLexico.AñadirEstado(true, "@", "int", "q115");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q116");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q117");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q118");
        AnalizadorLexico.AñadirEstado(true, "ɷ", "double", "q119");

        AnalizadorLexico.AñadirEstado(true, "i", "id", "q120");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q121");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q122");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q123");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q124");
        AnalizadorLexico.AñadirEstado(true, "?", "string", "q125");
        AnalizadorLexico.AñadirEstado(true, ",", ",", "q126");
        AnalizadorLexico.AñadirEstado(true, ".", ".", "q127");

        AnalizadorLexico.AñadirEstado(true, "i", "id", "q128");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q129");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q130");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q131");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q132");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q133");
        AnalizadorLexico.AñadirEstado(true, "ɐ", "boolean", "q134");

        AnalizadorLexico.AñadirEstado(true, "{", "{", "q135");
        AnalizadorLexico.AñadirEstado(true, "}", "}", "q136");

        AnalizadorLexico.AñadirEstado(true, "i", "id", "q137");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q138");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q139");
        AnalizadorLexico.AñadirEstado(true, "¡", "downto", "q140");

        AnalizadorLexico.AñadirEstado(true, "i", "id", "q141");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q142");
        AnalizadorLexico.AñadirEstado(true, "à", "pila", "q143");

        AnalizadorLexico.AñadirEstado(true, "i", "id", "q144");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q145");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q146");
        AnalizadorLexico.AñadirEstado(true, "ē", "lista", "q147");

        AnalizadorLexico.AñadirEstado(true, "i", "id", "q148");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q149");
        AnalizadorLexico.AñadirEstado(true, "i", "id", "q150");
        AnalizadorLexico.AñadirEstado(true, "ď", "cola", "q151");

        //HASTA AQUI
        AnalizadorLexico.AñadiInicial("q0");

        AnalizadorLexico.AñadirArista("q0", "q1", "\\+");
        AnalizadorLexico.AñadirArista("q0", "q2", "[^ietTFEwrudbpnaomslcf\\,\\.\\+\\'\\*\\/\\-\\<\\[\\]+\\>\\=\\┌\\┐\\└\\┘\\{\\}\\#\\(\\)\\[\\]^0-9]");
        AnalizadorLexico.AñadirArista("q0", "q3", "'");
        AnalizadorLexico.AñadirArista("q0", "q126", ",");
        AnalizadorLexico.AñadirArista("q0", "q4", "\\*");
        AnalizadorLexico.AñadirArista("q0", "q5", "\\/");
        AnalizadorLexico.AñadirArista("q0", "q113", "\\-");
        AnalizadorLexico.AñadirArista("q0", "q135", "\\{");
        AnalizadorLexico.AñadirArista("q0", "q136", "\\}");
        AnalizadorLexico.AñadirArista("q2", "q2", "([a-zA-Z]|[0-9])");

        AnalizadorLexico.AñadirArista("q0", "q6", "\\<");
        AnalizadorLexico.AñadirArista("q6", "q8", "\\=");
        AnalizadorLexico.AñadirArista("q6", "q10", "\\>");
        AnalizadorLexico.AñadirArista("q6", "q11", "\\-");
        AnalizadorLexico.AñadirArista("q0", "q13", "[ ]+");
        AnalizadorLexico.AñadirArista("q13", "q13", "[ ]+");
        AnalizadorLexico.AñadirArista("q0", "q7", "\\>");
        AnalizadorLexico.AñadirArista("q7", "q9", "\\=");
        AnalizadorLexico.AñadirArista("q0", "q12", "\\=");

        AnalizadorLexico.AñadirArista("q0", "q14", "\\┌");
        AnalizadorLexico.AñadirArista("q0", "q15", "\\┐");
        AnalizadorLexico.AñadirArista("q0", "q16", "\\└");
        AnalizadorLexico.AñadirArista("q0", "q17", "\\┘");

        AnalizadorLexico.AñadirArista("q0", "q19", "\\#");
        AnalizadorLexico.AñadirArista("q0", "q20", "\\(");
        AnalizadorLexico.AñadirArista("q0", "q21", "\\)");
        AnalizadorLexico.AñadirArista("q0", "q22", "\\[");
        AnalizadorLexico.AñadirArista("q0", "q23", "\\]");

        //if int
        AnalizadorLexico.AñadirArista("q0", "q24", "i");
        AnalizadorLexico.AñadirArista("q24", "q25", "f");
        AnalizadorLexico.AñadirArista("q24", "q2", "((?!f|n)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q25", "q2", "([a-zA-Z]|[0-9])");

        AnalizadorLexico.AñadirArista("q24", "q114", "n");
        AnalizadorLexico.AñadirArista("q114", "q115", "t");
        AnalizadorLexico.AñadirArista("q114", "q2", "((?!t)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q115", "q2", "([a-zA-Z]|[0-9])");

        //else end
        AnalizadorLexico.AñadirArista("q0", "q26", "e");
        AnalizadorLexico.AñadirArista("q26", "q27", "l");
        AnalizadorLexico.AñadirArista("q26", "q2", "((?!l|n)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q27", "q28", "s");
        AnalizadorLexico.AñadirArista("q27", "q2", "((?!s)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q28", "q29", "e");
        AnalizadorLexico.AñadirArista("q28", "q2", "((?!e)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q29", "q2", "([a-zA-Z]|[0-9])");

        AnalizadorLexico.AñadirArista("q26", "q30", "n");
        AnalizadorLexico.AñadirArista("q30", "q31", "d");
        AnalizadorLexico.AñadirArista("q30", "q2", "((?!d)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q31", "q2", "([a-zA-Z]|[0-9])");

        //then to
        AnalizadorLexico.AñadirArista("q0", "q32", "t");
        AnalizadorLexico.AñadirArista("q32", "q33", "h");
        AnalizadorLexico.AñadirArista("q32", "q2", "((?!h|o)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q33", "q34", "e");
        AnalizadorLexico.AñadirArista("q33", "q2", "((?!e)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q34", "q35", "n");
        AnalizadorLexico.AñadirArista("q34", "q2", "((?!n)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q35", "q2", "([a-zA-Z]|[0-9])");

        AnalizadorLexico.AñadirArista("q32", "q36", "o");
        AnalizadorLexico.AñadirArista("q36", "q2", "([a-zA-Z]|[0-9])");

        //for function
        AnalizadorLexico.AñadirArista("q0", "q37", "f");
        AnalizadorLexico.AñadirArista("q37", "q38", "o");
        AnalizadorLexico.AñadirArista("q37", "q2", "((?!o|u)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q38", "q39", "r");
        AnalizadorLexico.AñadirArista("q38", "q2", "((?!r)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q39", "q2", "([a-zA-Z]|[0-9])");

        AnalizadorLexico.AñadirArista("q37", "q40", "u");
        AnalizadorLexico.AñadirArista("q40", "q41", "n");
        AnalizadorLexico.AñadirArista("q40", "q2", "((?!n)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q41", "q42", "c");
        AnalizadorLexico.AñadirArista("q41", "q2", "((?!c)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q42", "q43", "t");
        AnalizadorLexico.AñadirArista("q42", "q2", "((?!t)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q43", "q44", "i");
        AnalizadorLexico.AñadirArista("q43", "q2", "((?!i)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q44", "q45", "o");
        AnalizadorLexico.AñadirArista("q44", "q2", "((?!o)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q45", "q46", "n");
        AnalizadorLexico.AñadirArista("q45", "q2", "((?!n)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q46", "q2", "([a-zA-Z]|[0-9])");

        //T
        AnalizadorLexico.AñadirArista("q0", "q47", "T");
        AnalizadorLexico.AñadirArista("q47", "q2", "([a-zA-Z]|[0-9])");
        //F
        AnalizadorLexico.AñadirArista("q0", "q48", "F");
        AnalizadorLexico.AñadirArista("q48", "q2", "([a-zA-Z]|[0-9])");
        //E ES
        AnalizadorLexico.AñadirArista("q0", "q49", "E");
        AnalizadorLexico.AñadirArista("q49", "q2", "((?!S)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q49", "q50", "S");
        AnalizadorLexico.AñadirArista("q50", "q2", "([a-zA-Z]|[0-9])");
        //while
        AnalizadorLexico.AñadirArista("q0", "q51", "w");
        AnalizadorLexico.AñadirArista("q51", "q52", "h");
        AnalizadorLexico.AñadirArista("q51", "q2", "((?!h)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q52", "q53", "i");
        AnalizadorLexico.AñadirArista("q52", "q2", "((?!i)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q53", "q54", "l");
        AnalizadorLexico.AñadirArista("q53", "q2", "((?!l)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q54", "q55", "e");
        AnalizadorLexico.AñadirArista("q54", "q2", "((?!e)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q55", "q2", "([a-zA-Z]|[0-9])");

        //repeat return
        AnalizadorLexico.AñadirArista("q0", "q56", "r");
        AnalizadorLexico.AñadirArista("q56", "q57", "e");
        AnalizadorLexico.AñadirArista("q56", "q2", "((?!e)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q57", "q58", "p");
        AnalizadorLexico.AñadirArista("q57", "q2", "((?!p|t)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q58", "q59", "e");
        AnalizadorLexico.AñadirArista("q58", "q2", "((?!e)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q59", "q60", "a");
        AnalizadorLexico.AñadirArista("q59", "q2", "((?!a)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q60", "q61", "t");
        AnalizadorLexico.AñadirArista("q60", "q2", "((?!t)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q61", "q2", "([a-zA-Z]|[0-9])");

        AnalizadorLexico.AñadirArista("q57", "q62", "t");
        AnalizadorLexico.AñadirArista("q62", "q63", "u");
        AnalizadorLexico.AñadirArista("q62", "q2", "((?!u)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q63", "q64", "r");
        AnalizadorLexico.AñadirArista("q63", "q2", "((?!r)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q64", "q65", "n");
        AnalizadorLexico.AñadirArista("q64", "q2", "((?!n)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q65", "q2", "([a-zA-Z]|[0-9])");

        //until
        AnalizadorLexico.AñadirArista("q0", "q66", "u");
        AnalizadorLexico.AñadirArista("q66", "q67", "n");
        AnalizadorLexico.AñadirArista("q66", "q2", "((?!n)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q67", "q68", "t");
        AnalizadorLexico.AñadirArista("q67", "q2", "((?!t)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q68", "q69", "i");
        AnalizadorLexico.AñadirArista("q68", "q2", "((?!i)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q69", "q70", "l");
        AnalizadorLexico.AñadirArista("q69", "q2", "((?!l)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q70", "q2", "([a-zA-Z]|[0-9])");
        //do div
        AnalizadorLexico.AñadirArista("q0", "q71", "d");
        AnalizadorLexico.AñadirArista("q71", "q73", "i");
        AnalizadorLexico.AñadirArista("q71", "q2", "((?!i|o)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q73", "q74", "v");
        AnalizadorLexico.AñadirArista("q73", "q2", "((?!v)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q74", "q2", "([a-zA-Z]|[0-9])");

        AnalizadorLexico.AñadirArista("q71", "q72", "o");
        AnalizadorLexico.AñadirArista("q72", "q116", "u");
        AnalizadorLexico.AñadirArista("q72", "q2", "((?!u|w)[a-zA-Z]|[0-9])");

        AnalizadorLexico.AñadirArista("q116", "q117", "b");
        AnalizadorLexico.AñadirArista("q116", "q2", "((?!b)[a-zA-Z]|[0-9])");

        AnalizadorLexico.AñadirArista("q117", "q118", "l");
        AnalizadorLexico.AñadirArista("q117", "q2", "((?!l)[a-zA-Z]|[0-9])");

        AnalizadorLexico.AñadirArista("q118", "q119", "e");
        AnalizadorLexico.AñadirArista("q118", "q2", "((?!e)[a-zA-Z]|[0-9])");

        AnalizadorLexico.AñadirArista("q119", "q2", "([a-zA-Z]|[0-9])");

        AnalizadorLexico.AñadirArista("q72", "q137", "w");
        AnalizadorLexico.AñadirArista("q137", "q138", "n");
        AnalizadorLexico.AñadirArista("q137", "q2", "((?!n)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q138", "q139", "t");
        AnalizadorLexico.AñadirArista("q138", "q2", "((?!t)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q139", "q140", "o");
        AnalizadorLexico.AñadirArista("q139", "q2", "((?!o)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q140", "q2", "([a-zA-Z]|[0-9])");
        //begin boolean
        AnalizadorLexico.AñadirArista("q0", "q75", "b");
        AnalizadorLexico.AñadirArista("q75", "q76", "e");
        AnalizadorLexico.AñadirArista("q75", "q2", "((?!e|o)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q76", "q77", "g");
        AnalizadorLexico.AñadirArista("q76", "q2", "((?!g)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q77", "q78", "i");
        AnalizadorLexico.AñadirArista("q77", "q2", "((?!i)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q78", "q79", "n");
        AnalizadorLexico.AñadirArista("q78", "q2", "((?!n)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q79", "q2", "([a-zA-Z]|[0-9])");

        AnalizadorLexico.AñadirArista("q75", "q129", "o");
        AnalizadorLexico.AñadirArista("q129", "q130", "o");
        AnalizadorLexico.AñadirArista("q129", "q2", "((?!o)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q130", "q131", "l");
        AnalizadorLexico.AñadirArista("q130", "q2", "((?!l)[a-zA-Z]|[0-9])");

        AnalizadorLexico.AñadirArista("q131", "q132", "e");
        AnalizadorLexico.AñadirArista("q131", "q2", "((?!e)[a-zA-Z]|[0-9])");

        AnalizadorLexico.AñadirArista("q132", "q133", "a");
        AnalizadorLexico.AñadirArista("q132", "q2", "((?!a)[a-zA-Z]|[0-9])");

        AnalizadorLexico.AñadirArista("q133", "q134", "n");
        AnalizadorLexico.AñadirArista("q133", "q2", "((?!n)[a-zA-Z]|[0-9])");

        AnalizadorLexico.AñadirArista("q134", "q2", "([a-zA-Z]|[0-9])");
        //procedure pila
        AnalizadorLexico.AñadirArista("q0", "q80", "p");
        AnalizadorLexico.AñadirArista("q80", "q81", "r");
        AnalizadorLexico.AñadirArista("q80", "q2", "((?!r|i)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q81", "q82", "o");
        AnalizadorLexico.AñadirArista("q81", "q2", "((?!o)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q82", "q83", "c");
        AnalizadorLexico.AñadirArista("q82", "q2", "((?!c)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q83", "q84", "e");
        AnalizadorLexico.AñadirArista("q83", "q2", "((?!e)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q84", "q85", "d");
        AnalizadorLexico.AñadirArista("q84", "q2", "((?!d)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q85", "q86", "u");
        AnalizadorLexico.AñadirArista("q85", "q2", "((?!u)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q86", "q87", "r");
        AnalizadorLexico.AñadirArista("q86", "q2", "((?!r)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q87", "q88", "e");
        AnalizadorLexico.AñadirArista("q87", "q2", "((?!e)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q88", "q2", "([a-zA-Z]|[0-9])");

        AnalizadorLexico.AñadirArista("q80", "q141", "i");
        AnalizadorLexico.AñadirArista("q141", "q142", "l");
        AnalizadorLexico.AñadirArista("q141", "q2", "((?!l)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q142", "q143", "a");
        AnalizadorLexico.AñadirArista("q142", "q2", "((?!a)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q143", "q2", "([a-zA-Z]|[0-9])");
        //null not
        AnalizadorLexico.AñadirArista("q0", "q89", "n");
        AnalizadorLexico.AñadirArista("q89", "q90", "u");
        AnalizadorLexico.AñadirArista("q89", "q2", "((?!u|o)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q90", "q91", "l");
        AnalizadorLexico.AñadirArista("q90", "q2", "((?!l)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q91", "q92", "l");
        AnalizadorLexico.AñadirArista("q91", "q2", "((?!l)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q92", "q2", "([a-zA-Z]|[0-9])");

        AnalizadorLexico.AñadirArista("q89", "q93", "o");
        AnalizadorLexico.AñadirArista("q89", "q2", "((?!o)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q93", "q94", "t");
        AnalizadorLexico.AñadirArista("q93", "q2", "((?!t)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q94", "q2", "([a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q0", "q126", ",");

        AnalizadorLexico.AñadirArista("q0", "q95", "l");
        AnalizadorLexico.AñadirArista("q95", "q144", "i");
        AnalizadorLexico.AñadirArista("q95", "q2", "((?!i)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q144", "q145", "s");
        AnalizadorLexico.AñadirArista("q144", "q2", "((?!s)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q145", "q146", "t");
        AnalizadorLexico.AñadirArista("q145", "q2", "((?!t)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q146", "q147", "a");
        AnalizadorLexico.AñadirArista("q146", "q2", "((?!a)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q147", "q2", "([a-zA-Z]|[0-9])");
        //lenght

        //and
        AnalizadorLexico.AñadirArista("q0", "q101", "a");
        AnalizadorLexico.AñadirArista("q101", "q102", "n");
        AnalizadorLexico.AñadirArista("q101", "q2", "((?!n)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q102", "q103", "d");
        AnalizadorLexico.AñadirArista("q102", "q2", "((?!d)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q103", "q2", "([a-zA-Z]|[0-9])");

        //or
        AnalizadorLexico.AñadirArista("q0", "q104", "o");
        AnalizadorLexico.AñadirArista("q104", "q105", "r");
        AnalizadorLexico.AñadirArista("q104", "q2", "((?!r)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q105", "q2", "([a-zA-Z]|[0-9])");
        //mod
        AnalizadorLexico.AñadirArista("q0", "q106", "m");
        AnalizadorLexico.AñadirArista("q106", "q107", "o");
        AnalizadorLexico.AñadirArista("q106", "q2", "((?!o)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q107", "q108", "d");
        AnalizadorLexico.AñadirArista("q107", "q2", "((?!d)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q108", "q2", "([a-zA-Z]|[0-9])");

        //entero
        AnalizadorLexico.AñadirArista("q0", "q109", "[0-9]");
        AnalizadorLexico.AñadirArista("q109", "q109", "[0-9]");
        AnalizadorLexico.AñadirArista("q109", "q110", "[\\.]");
        //decimal
        AnalizadorLexico.AñadirArista("q110", "q110", "[0-9]");
        /*
        AnalizadorLexico.AñadirArista("q3", "q111", "[^'].*");
        AnalizadorLexico.AñadirArista("q3", "q112", "\\'");
        AnalizadorLexico.AñadirArista("q111", "q111", "[^'].*");
        AnalizadorLexico.AñadirArista("q111", "q112", "\\'");
         */

        AnalizadorLexico.AñadirArista("q0", "q120", "s");
        AnalizadorLexico.AñadirArista("q120", "q121", "t");
        AnalizadorLexico.AñadirArista("q120", "q2", "((?!t)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q121", "q122", "r");
        AnalizadorLexico.AñadirArista("q121", "q2", "((?!r)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q122", "q123", "i");
        AnalizadorLexico.AñadirArista("q122", "q2", "((?!i)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q123", "q124", "n");
        AnalizadorLexico.AñadirArista("q123", "q2", "((?!n)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q124", "q125", "g");
        AnalizadorLexico.AñadirArista("q124", "q2", "((?!g)[a-zA-Z]|[0-9])");

        AnalizadorLexico.AñadirArista("q125", "q2", "([a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q0", "q127", "\\.");

        AnalizadorLexico.AñadirArista("q0", "q148", "c");
        AnalizadorLexico.AñadirArista("q148", "q149", "o");
        AnalizadorLexico.AñadirArista("q148", "q2", "((?!o)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q149", "q150", "l");
        AnalizadorLexico.AñadirArista("q149", "q2", "((?!l)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q150", "q151", "a");
        AnalizadorLexico.AñadirArista("q150", "q2", "((?!a)[a-zA-Z]|[0-9])");
        AnalizadorLexico.AñadirArista("q151", "q2", "([a-zA-Z]|[0-9])");
        // AnalizadorLexico.AñadirArista("q113", "q109", "[0-9]");

        //AnalizadorLexico.Ver();
    }

    public void CrearAnalizadorSintactico() {
        try {
            LinkedList<String> Tokens = new LinkedList<String>();
            Tokens.add("*");
            Tokens.add("+");
            Tokens.add(",");
            Tokens.add("-");
            Tokens.add("/");
            Tokens.add("?");
            Tokens.add("@");
            Tokens.add("a");
            Tokens.add("b");
            Tokens.add("c");
            Tokens.add("d");
            Tokens.add("e");
            Tokens.add("f");
            Tokens.add("g");
            Tokens.add("h");
            Tokens.add("i");//
            Tokens.add("j");
            Tokens.add("k");
            Tokens.add("l");
            Tokens.add("m");
            Tokens.add("n");
            Tokens.add("p");
            Tokens.add("q");
            Tokens.add("r");
            Tokens.add("s");
            Tokens.add("v");
            Tokens.add("w");
            Tokens.add("x");
            Tokens.add("y");
            Tokens.add("z");
            Tokens.add("{");
            Tokens.add("}");
            Tokens.add("¡");
            Tokens.add("¤");
            Tokens.add("µ");
            Tokens.add("à");
            Tokens.add("á");

            Tokens.add("æ");

            Tokens.add("ç");
            Tokens.add("é");
            Tokens.add("í");
            Tokens.add("ñ");
            Tokens.add("ó");
            Tokens.add("ú");
            Tokens.add("ď");
            Tokens.add("ē");
            Tokens.add("ĝ");
            Tokens.add("ŵ");
            Tokens.add("ɐ");
            Tokens.add("ɘ");
            Tokens.add("ɷ");
            Tokens.add("┌");
            Tokens.add("┐");
            Tokens.add("└");
            Tokens.add("┘");

            Tokens.add("$");
            Tokens.add("A");
            Tokens.add("B");
            Tokens.add("C");
            Tokens.add("D");
            Tokens.add("E");
            Tokens.add("F");
            Tokens.add("G");
            Tokens.add("H");
            Tokens.add("I");
            Tokens.add("J");
            Tokens.add("K");
            Tokens.add("L");
            Tokens.add("M");
            Tokens.add("N");
            Tokens.add("O");
            Tokens.add("P");
            Tokens.add("Q");
            Tokens.add("R");
            Tokens.add("S");
            Tokens.add("T");
            Tokens.add("U");
            Tokens.add("V");
            Tokens.add("W");
            Tokens.add("X");
            Tokens.add("Y");
            Tokens.add("Z");
            Tokens.add("Ñ");
            Tokens.add("Ą");
            LinkedList<LinkedList<String>> producciones = new LinkedList<>();
            producciones.add(llenar("A U"));
            producciones.add(llenar("C µ D"));
            producciones.add(llenar("C ĝ D"));
            producciones.add(llenar("C i D"));
            producciones.add(llenar("C I"));
            producciones.add(llenar("C r C s D"));
            producciones.add(llenar("D + C"));
            producciones.add(llenar("D * C"));
            producciones.add(llenar("D / C"));
            producciones.add(llenar("D - C"));
            producciones.add(llenar("B i ç E"));
            producciones.add(llenar("E y F"));
            producciones.add(llenar("E z F"));
            producciones.add(llenar("E i F"));
            producciones.add(llenar("E r E s F"));
            producciones.add(llenar("F w E"));
            producciones.add(llenar("F v E"));
            producciones.add(llenar("G a r H s c l L d M"));
            producciones.add(llenar("H µ J"));
            producciones.add(llenar("H ĝ J"));
            producciones.add(llenar("H i J"));
            producciones.add(llenar("H y K"));
            producciones.add(llenar("H z K"));
            producciones.add(llenar("H r H s K"));
            producciones.add(llenar("H i K"));
            producciones.add(llenar("H x i K"));
            producciones.add(llenar("H x r H s K"));
            producciones.add(llenar("K v H"));
            producciones.add(llenar("K w H"));
            producciones.add(llenar("J á H"));
            producciones.add(llenar("J í H"));
            producciones.add(llenar("J é H"));
            producciones.add(llenar("J ó H"));
            producciones.add(llenar("J ú H"));
            producciones.add(llenar("L G L"));
            producciones.add(llenar("L B L"));
            producciones.add(llenar("M G"));
            producciones.add(llenar("M N"));
            producciones.add(llenar("N b l L d"));
            producciones.add(llenar("B i ç C"));
            producciones.add(llenar("A R"));
            producciones.add(llenar("Ñ e r H s k l L d"));
            producciones.add(llenar("O g L h r H s"));
            producciones.add(llenar("B i p C q ç C"));
            producciones.add(llenar("C i p C q D"));
            producciones.add(llenar("L Ñ L"));
            producciones.add(llenar("L P L"));
            producciones.add(llenar("U n i r S s l L ñ C d"));
            producciones.add(llenar("S ɘ ? i p q T"));
            producciones.add(llenar("S ɘ ɷ i p q T"));
            producciones.add(llenar("S ŵ @ i T"));
            producciones.add(llenar("R m i r S s l L d"));
            producciones.add(llenar("S ŵ ɷ i T"));
            producciones.add(llenar("S ŵ ? i T"));
            producciones.add(llenar("S ɘ @ i T"));
            producciones.add(llenar("S ɘ ɷ i T"));
            producciones.add(llenar("S ɘ ? i T"));
            producciones.add(llenar("T , S"));
            producciones.add(llenar("S ŵ @ i p q T"));
            producciones.add(llenar("S ŵ ɷ i p q T"));
            producciones.add(llenar("S ŵ ? i p q T"));
            producciones.add(llenar("S ɘ @ i p q T"));
            producciones.add(llenar("P f i ç C j C k l L d"));
            producciones.add(llenar("A Ñ"));
            producciones.add(llenar("C i r i s D"));
            producciones.add(llenar("L ñ C L"));
            producciones.add(llenar("A P"));
            producciones.add(llenar("S ɘ ɐ i p q T"));
            producciones.add(llenar("S ŵ ɐ i T"));
            producciones.add(llenar("S ɘ ɐ i T"));
            producciones.add(llenar("S ŵ ɐ i p q T"));
            producciones.add(llenar("J æ H"));
            producciones.add(llenar("H i p i q K"));
            producciones.add(llenar("H i p µ q K"));
            producciones.add(llenar("L O L"));
            producciones.add(llenar("B @ i ç C"));
            producciones.add(llenar("B ɷ i ç C"));
            producciones.add(llenar("B ? i ç I"));
            producciones.add(llenar("I ¤"));
            producciones.add(llenar("I ¤ + I"));
            producciones.add(llenar("I i"));
            producciones.add(llenar("I i + I"));
            producciones.add(llenar("B ɐ i ç E"));
            producciones.add(llenar("B ɐ i ç H"));
            producciones.add(llenar("Q i r s"));
            producciones.add(llenar("Q i r V s"));
            producciones.add(llenar("V i"));
            producciones.add(llenar("V i , V"));
            producciones.add(llenar("V µ"));
            producciones.add(llenar("V µ , V"));
            producciones.add(llenar("V ĝ"));
            producciones.add(llenar("V ĝ , V"));
            producciones.add(llenar("V y"));
            producciones.add(llenar("V y , V"));
            producciones.add(llenar("V z"));
            producciones.add(llenar("V z , V"));
            producciones.add(llenar("V ¤"));
            producciones.add(llenar("V ¤ , V"));
            producciones.add(llenar("V Q V"));
            producciones.add(llenar("B i ç Q"));
            producciones.add(llenar("B i p C q ç Q"));
            producciones.add(llenar("B @ i ç Q"));
            producciones.add(llenar("B ɷ i ç Q"));
            producciones.add(llenar("B ? i ç Q"));
            producciones.add(llenar("B ɐ i ç Q"));
            producciones.add(llenar("C Q D"));
            producciones.add(llenar("H Q J"));
            producciones.add(llenar("H Q K"));
            producciones.add(llenar("V C , V"));
            producciones.add(llenar("V C"));
            producciones.add(llenar("B @ p i q i"));
            producciones.add(llenar("B @ p µ q i"));
            producciones.add(llenar("B ɷ p i q i"));
            producciones.add(llenar("B ɷ p µ q i"));
            producciones.add(llenar("B ? p i q i"));
            producciones.add(llenar("B ? p µ q i"));
            producciones.add(llenar("B @ p i q i ç { W }"));
            producciones.add(llenar("W µ"));
            producciones.add(llenar("W µ , W"));
            producciones.add(llenar("W i"));
            producciones.add(llenar("W i , W"));
            producciones.add(llenar("B ɷ p i q i ç { X }"));
            producciones.add(llenar("X ĝ"));
            producciones.add(llenar("X ĝ , X"));
            producciones.add(llenar("X i , X"));
            producciones.add(llenar("B ? p i q i ç { Y }"));
            producciones.add(llenar("Y i"));
            producciones.add(llenar("Y i , Y"));
            producciones.add(llenar("Y ¤"));
            producciones.add(llenar("Y ¤ , Y"));
            producciones.add(llenar("B ɐ p i q i ç { Z }"));
            producciones.add(llenar("Z i"));
            producciones.add(llenar("Z i , Z"));
            producciones.add(llenar("Z y"));
            producciones.add(llenar("Z y , Z"));
            producciones.add(llenar("Z z"));
            producciones.add(llenar("Z z , Z"));
            producciones.add(llenar("X i"));
            producciones.add(llenar("B ɐ i p i q"));
            producciones.add(llenar("B ɐ i p µ q"));
            producciones.add(llenar("L Q L"));
            producciones.add(llenar("C ┌ µ D ┐"));
            producciones.add(llenar("C ┌ ĝ D ┐"));
            producciones.add(llenar("C ┌ i D ┐"));
            producciones.add(llenar("C └ µ D ┘"));
            producciones.add(llenar("C └ ĝ D ┘"));
            producciones.add(llenar("C └ i D ┘"));
            producciones.add(llenar("C └ r C s D ┘"));
            producciones.add(llenar("C ┌ r C s D ┐"));
            producciones.add(llenar("I µ"));
            producciones.add(llenar("I µ + I"));
            producciones.add(llenar("I ĝ + I"));
            producciones.add(llenar("I ĝ"));
            producciones.add(llenar("A U A"));
            producciones.add(llenar("P f i ç C ¡ C k l L d"));
            producciones.add(llenar("L Ą L"));
            producciones.add(llenar("Ą ď @ i"));
            producciones.add(llenar("Ą ď ɷ i"));
            producciones.add(llenar("Ą ď ? i"));
            producciones.add(llenar("Ą ď ɐ i"));
            producciones.add(llenar("Ą ē @ i"));
            producciones.add(llenar("Ą ē ɷ i"));
            producciones.add(llenar("Ą ē ? i"));
            producciones.add(llenar("Ą ē ɐ i"));
            producciones.add(llenar("Ą à @ i"));
            producciones.add(llenar("Ą à ɷ i"));
            producciones.add(llenar("Ą à ? i"));
            producciones.add(llenar("Ą à ɐ i"));
            producciones.add(llenar("A R A"));
            producciones.add(llenar("H i p µ q J"));
            producciones.add(llenar("H i p i q J"));
            producciones.add(llenar("C µ"));
            producciones.add(llenar("C ĝ"));
            producciones.add(llenar("C i"));
            producciones.add(llenar("C r C s"));
            producciones.add(llenar("E y"));
            producciones.add(llenar("E z"));
            producciones.add(llenar("E i"));
            producciones.add(llenar("E r E s"));
            producciones.add(llenar("G a r H s c l L d"));
            producciones.add(llenar("G a r H s c l d"));
            producciones.add(llenar("G a r H s c l d M"));
            producciones.add(llenar("H µ"));
            producciones.add(llenar("H ĝ"));
            producciones.add(llenar("H i"));
            producciones.add(llenar("H y"));
            producciones.add(llenar("H z"));
            producciones.add(llenar("H r H s"));
            producciones.add(llenar("H x i"));
            producciones.add(llenar("H x r H s"));
            producciones.add(llenar("L G"));
            producciones.add(llenar("L B"));
            producciones.add(llenar("N b l d"));
            producciones.add(llenar("Ñ e r H s k l d"));
            producciones.add(llenar("O g h r H s"));
            producciones.add(llenar("C i p C q"));
            producciones.add(llenar("L Ñ"));
            producciones.add(llenar("L P"));
            producciones.add(llenar("U n i r S s l ñ C d"));
            producciones.add(llenar("U n i r s l L ñ C d"));
            producciones.add(llenar("U n i r s l ñ C d"));
            producciones.add(llenar("S ɘ ? i p q"));
            producciones.add(llenar("S ɘ ɷ i p q"));
            producciones.add(llenar("S ŵ @ i"));
            producciones.add(llenar("R m i r S s l d"));
            producciones.add(llenar("R m i r s l L d"));
            producciones.add(llenar("R m i r s l d"));
            producciones.add(llenar("S ŵ ɷ i"));
            producciones.add(llenar("S ŵ ? i"));
            producciones.add(llenar("S ɘ @ i"));
            producciones.add(llenar("S ɘ ɷ i"));
            producciones.add(llenar("S ɘ ? i"));
            producciones.add(llenar("T ,"));
            producciones.add(llenar("S ŵ @ i p q"));
            producciones.add(llenar("S ŵ ɷ i p q"));
            producciones.add(llenar("S ŵ ? i p q"));
            producciones.add(llenar("S ɘ @ i p q"));
            producciones.add(llenar("P f i ç C j C k l d"));
            producciones.add(llenar("C i r i s"));
            producciones.add(llenar("L ñ C"));
            producciones.add(llenar("S ɘ ɐ i p q"));
            producciones.add(llenar("S ŵ ɐ i"));
            producciones.add(llenar("S ɘ ɐ i"));
            producciones.add(llenar("S ŵ ɐ i p q"));
            producciones.add(llenar("H i p i q"));
            producciones.add(llenar("H i p µ q"));
            producciones.add(llenar("L O"));
            producciones.add(llenar("C Q"));
            producciones.add(llenar("H Q"));
            producciones.add(llenar("L Q"));
            producciones.add(llenar("C ┌ µ ┐"));
            producciones.add(llenar("C ┌ ĝ ┐"));
            producciones.add(llenar("C ┌ i ┐"));
            producciones.add(llenar("C └ µ ┘"));
            producciones.add(llenar("C └ ĝ ┘"));
            producciones.add(llenar("C └ i ┘"));
            producciones.add(llenar("C └ r C s ┘"));
            producciones.add(llenar("C ┌ r C s ┐"));
            producciones.add(llenar("P f i ç C ¡ C k l d"));
            producciones.add(llenar("L Ą"));

            String[][] m = null;

            m = leer();

            String[][] m2 = acomodar(m);
            AnalizadorSintactico = new SRL(Tokens, m2, producciones);
        }
        catch (IOException ex) {
            Logger.getLogger(Editor.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (BiffException ex) {
            Logger.getLogger(Editor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void PartirFuncionesYProcedimientos(LinkedList<Token> Input) {
        System.out.println(Input.size());
        while (Input.getFirst().getNombre().equals("fl")) {
            Input.removeFirst();
        }

        FPExistentes = new LinkedList<LinkedList<Token>>();
        LinkedList<Token> aux = new LinkedList<Token>();

        for (int i = 0; i < Input.size() - 1; i++) {
            aux.add(Input.get(i));
            if ((i == (Input.size() - 2)) || (Input.get(i + 1).getNombre().equals("m") || Input.get(i + 1).getNombre().equals("n"))) {
                if (i == (Input.size() - 2)) {
                    aux.add(Input.get(i + 1));
                }

                FPExistentes.add(aux);
                aux = new LinkedList<Token>();
            }
        }
        System.out.println("\n # de Procesos y Funciones " + FPExistentes.size());
        for (int i = 0; i < FPExistentes.size(); i++) {
            System.out.println("Procedimiento " + (i + 1));
            aux = new LinkedList<Token>();
            aux = FPExistentes.get(i);
            for (int j = 0; j < aux.size(); j++) {
                System.out.print(aux.get(j).getNombre());
            }
            System.out.println("\n ______");
        }
    }

    public void guardarArchivo() {
        try {
            String nombre = JOptionPane.showInputDialog("Ingrese nombre del nuevo archivo");
            JFileChooser file = new JFileChooser();
            file.showSaveDialog(this);
            String ruta = "" + file.getCurrentDirectory();
            BufferedWriter save = new BufferedWriter(new FileWriter(ruta + ".\\" + nombre + ".txt"));
            String contenido = txt.getText();
            /*guardamos el archivo y le damos el formato directamente,
           * si queremos que se guarde en formato doc lo definimos como .doc*/
            //System.out.println("nombre "+nombres.get(indice)+" contenido "+textpane.get(indice).getText()+" ruta "+ruta+nombres.get(indice)+".txt");
            save.write(contenido);
            save.close();
            JOptionPane.showMessageDialog(null, "El archivo se a guardado Exitosamente", "Información", JOptionPane.INFORMATION_MESSAGE);

        }
        catch (IOException ex) {
            JOptionPane.showMessageDialog(null,
                    "Su archivo no se ha guardado",
                    "Advertencia", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void abrirArchivo() {
        try {
            JFileChooser chooser = new JFileChooser();
            chooser.setMultiSelectionEnabled(true);
            chooser.showOpenDialog(this);
            File[] files = chooser.getSelectedFiles();
            for (int i = 0; i < files.length; i++) {
                System.out.println("" + files[i].getName());
                if (files[i] != null) {
                    String aux = "";
                    String texto = "";
                    FileReader archivos = new FileReader(files[i]);
                    BufferedReader lee = new BufferedReader(archivos);
                    while ((aux = lee.readLine()) != null) {
                        texto += aux + "\n";
                    }
                    lee.close();
                    txt.setText(texto);

                }
            }
        }
        catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "" + ex + "" + "\n No se ha encontrado el archivo" + "ADVERTENCIA!!!");
        }
    }

    public void VisualizarId() {
        if (AnalizadorLexico == null) {
            System.out.println("El sistema no puede generar constantes aun");
        }
        else {
            LinkedList<Token> Lista = ModificarStirng(AnalizadorLexico.GenerarTonkens(txt.getText()));

            //Identificar constantes
            LinkedList<String> Identificadores = new LinkedList<>();
            for (int i = 0; i < Lista.size(); i++) {
                if (i > 0) {
                    if (!Lista.get(i - 1).getDato().equals("'")) {
                        if (Lista.get(i).getIdToken().equals("id")) {
                            if (!Identificadores.contains(Lista.get(i).getDato())) {
                                Identificadores.add(Lista.get(i).getDato());
                            }
                        }
                    }

                }

            }

            Object[] data = new Object[4];
            DefaultTableModel dtm = new DefaultTableModel();
            dtm.addColumn("Identificadores");

            for (int row = 0; row < Identificadores.size(); row++) {

                for (int column = 0; column < 1; column++) {
                    if (column == 0) {
                        data[column] = Identificadores.get(row);
                    }

                }
                dtm.addRow(data);
            }
            JFrame Visualizador = new JFrame();
            Visualizador.setSize(200, 700);
            JTable table = new JTable(dtm);
            table.setPreferredScrollableViewportSize(new Dimension(200, 80));
            //Creamos un scrollpanel y se lo agregamos a la tabla 
            JScrollPane scrollpane = new JScrollPane(table);
            //Agregamos el scrollpanel al contenedor 
            Visualizador.setLocation(1100, 0);
            Visualizador.add(scrollpane, BorderLayout.CENTER);
            Visualizador.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            Visualizador.setVisible(true);

        }
    }

    public void VisualizarTokens() {
        if (AnalizadorLexico == null) {
            System.out.println("me oprimieron");
        }
        else {

            LinkedList<Token> Lista = ModificarStirng(AnalizadorLexico.GenerarTonkens(txt.getText()));

            Object[] data = new Object[4];
            DefaultTableModel dtm = new DefaultTableModel();
            dtm.addColumn("Token");
            dtm.addColumn("Dato");
            dtm.addColumn("Linea");
            dtm.addColumn("Columna");
            for (int row = 0; row < Lista.size(); row++) {

                for (int column = 0; column < 4; column++) {
                    if (column == 0) {
                        data[column] = Lista.get(row).getIdToken();
                    }
                    else if (column == 1) {
                        data[column] = Lista.get(row).getDato();
                    }
                    else if (column == 2) {
                        data[column] = new Integer(Lista.get(row).getLinea());
                    }
                    else if (column == 3) {
                        data[column] = new Integer(Lista.get(row).getColumna());
                    }
                }
                dtm.addRow(data);
            }
            JFrame Visualizador = new JFrame();
            Visualizador.setSize(400, 700);
            JTable table = new JTable(dtm);
            table.setPreferredScrollableViewportSize(new Dimension(400, 80));
            //Creamos un scrollpanel y se lo agregamos a la tabla 
            JScrollPane scrollpane = new JScrollPane(table);
            //Agregamos el scrollpanel al contenedor 
            Visualizador.setLocation(1000, 0);
            Visualizador.add(scrollpane, BorderLayout.CENTER);
            Visualizador.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            Visualizador.setVisible(true);

        }
    }

    public void paint(Graphics g, int p0, int p1, Shape bounds, JTextPane c) {
        try {
            Rectangle r = c.modelToView(c.getCaretPosition());
            g.setColor(color);
            g.fillRect(0, r.y, c.getWidth(), r.height);

            if (lastView == null) {
                lastView = r;
            }
        }
        catch (BadLocationException ble) {
            System.out.println(ble);
        }
    }

    public void buscarpalabra(String texto) {
        if (texto.length() >= 1) {

            DefaultHighlightPainter highlightPainter = new DefaultHighlightPainter(Color.decode("#2279C7"));
            Highlighter h = txt.getHighlighter();
            h.removeAllHighlights();
            String text = txt.getText();
            String caracteres = texto;
            Pattern p = Pattern.compile("(?i)" + "else");
            Matcher m = p.matcher(text);
            while (m.find()) {
                try {
                    h.addHighlight(m.start(), m.end(), highlightPainter);
                }
                catch (BadLocationException ex) {
                    Logger.getLogger(Color.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        else {
            JOptionPane.showMessageDialog(null, "la palabra a buscar no puede ser vacia");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jCheckBox1 = new javax.swing.JCheckBox();
        jCheckBox2 = new javax.swing.JCheckBox();
        jToggleButton1 = new javax.swing.JToggleButton();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroup4 = new javax.swing.ButtonGroup();
        panel = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        btnNuevo = new javax.swing.JButton();
        btnAbrir = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnRun = new javax.swing.JButton();
        btnDedug = new javax.swing.JButton();
        btnArbol = new javax.swing.JButton();
        btnId = new javax.swing.JButton();
        btnTokens = new javax.swing.JButton();
        btnErrores = new javax.swing.JButton();
        isDebug = new javax.swing.JCheckBox();
        Breakpoint = new javax.swing.JCheckBox();
        MenuG = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();

        jCheckBox1.setText("jCheckBox1");

        jCheckBox2.setText("jCheckBox2");

        jToggleButton1.setText("jToggleButton1");

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(jList1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 0, 0));
        getContentPane().setLayout(null);

        panel.setBackground(new java.awt.Color(51, 51, 51));

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 800, Short.MAX_VALUE)
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 530, Short.MAX_VALUE)
        );

        getContentPane().add(panel);
        panel.setBounds(0, 50, 800, 530);

        jPanel1.setLayout(null);

        btnNuevo.setFont(new java.awt.Font("OCR A Extended", 0, 11)); // NOI18N
        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/nuevo.png"))); // NOI18N
        btnNuevo.setToolTipText("nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        jPanel1.add(btnNuevo);
        btnNuevo.setBounds(10, 10, 40, 30);

        btnAbrir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/abrir.png"))); // NOI18N
        btnAbrir.setToolTipText("Abrir");
        btnAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirActionPerformed(evt);
            }
        });
        jPanel1.add(btnAbrir);
        btnAbrir.setBounds(60, 10, 40, 30);

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/guardar.png"))); // NOI18N
        btnGuardar.setToolTipText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        jPanel1.add(btnGuardar);
        btnGuardar.setBounds(110, 10, 40, 30);

        btnRun.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/correr.png"))); // NOI18N
        btnRun.setToolTipText("Run");
        btnRun.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRunActionPerformed(evt);
            }
        });
        jPanel1.add(btnRun);
        btnRun.setBounds(160, 10, 40, 30);

        btnDedug.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/debug.png"))); // NOI18N
        btnDedug.setToolTipText("Debug");
        btnDedug.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDedugActionPerformed(evt);
            }
        });
        jPanel1.add(btnDedug);
        btnDedug.setBounds(210, 10, 40, 30);

        btnArbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/arbol.png"))); // NOI18N
        btnArbol.setToolTipText("Tree");
        btnArbol.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnArbolActionPerformed(evt);
            }
        });
        jPanel1.add(btnArbol);
        btnArbol.setBounds(260, 10, 40, 30);

        btnId.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/identificadores.png"))); // NOI18N
        btnId.setToolTipText("Identificadores");
        btnId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIdActionPerformed(evt);
            }
        });
        jPanel1.add(btnId);
        btnId.setBounds(310, 10, 40, 30);

        btnTokens.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/tokens.png"))); // NOI18N
        btnTokens.setToolTipText("Tokens");
        btnTokens.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTokensActionPerformed(evt);
            }
        });
        jPanel1.add(btnTokens);
        btnTokens.setBounds(360, 10, 40, 30);

        btnErrores.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/codigo.png"))); // NOI18N
        btnErrores.setToolTipText("Tokens");
        btnErrores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnErroresActionPerformed(evt);
            }
        });
        jPanel1.add(btnErrores);
        btnErrores.setBounds(410, 10, 40, 30);

        isDebug.setForeground(new java.awt.Color(255, 255, 255));
        isDebug.setText("DebugMode");
        isDebug.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                isDebugActionPerformed(evt);
            }
        });
        jPanel1.add(isDebug);
        isDebug.setBounds(640, 10, 120, 23);

        Breakpoint.setForeground(new java.awt.Color(255, 255, 255));
        Breakpoint.setText("BreakPoint");
        jPanel1.add(Breakpoint);
        Breakpoint.setBounds(560, 10, 110, 23);

        MenuG.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/break.png"))); // NOI18N
        MenuG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuGActionPerformed(evt);
            }
        });
        jPanel1.add(MenuG);
        MenuG.setBounds(510, 10, 40, 30);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/ruptura.png"))); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3);
        jButton3.setBounds(460, 10, 40, 30);

        jButton1.setBackground(new java.awt.Color(0, 0, 0));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(560, 50, 20, 0);

        jButton2.setMnemonic('X');
        jButton2.setToolTipText("");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(570, 40, 0, 10);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 650, 50);

        jMenuBar1.setBackground(new java.awt.Color(204, 204, 204));
        jMenuBar1.setBorder(null);
        jMenuBar1.setFont(new java.awt.Font("OCR A Extended", 0, 14)); // NOI18N

        jMenu1.setText("Archivo");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/nuevo.png"))); // NOI18N
        jMenuItem1.setText("Nuevo");
        jMenu1.add(jMenuItem1);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/abrir.png"))); // NOI18N
        jMenuItem2.setText("Abrir");
        jMenu1.add(jMenuItem2);

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_G, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/guardar.png"))); // NOI18N
        jMenuItem3.setText("Guardar");
        jMenu1.add(jMenuItem3);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Editar");
        jMenuBar1.add(jMenu2);

        jMenu3.setText("Ayuda");
        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        //  txt.repaint(0, 30, txt.getWidth(), lastView.height);
        LineP.setLinea(LineP.getLinea() + 15);
        try {
            LineP.resetHighlight1();
            txt.repaint();
        }
        catch (BadLocationException ex) {
            Logger.getLogger(Editor.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            Robot A = new Robot();
            //A.mousePress(MouseEvent.BUTTON1_MASK);

            // a.paint(Graphics g, 2, 70, Window.paint(g);, txt);
        }
        catch (AWTException ex) {
            Logger.getLogger(Editor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirActionPerformed
        System.out.println("cargar");
        abrirArchivo();

    }//GEN-LAST:event_btnAbrirActionPerformed

    private void btnIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIdActionPerformed
        VisualizarId();
    }//GEN-LAST:event_btnIdActionPerformed

    private void btnRunActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRunActionPerformed
        ContadorLineas = new TreeMap<>();

        TInicio = System.currentTimeMillis();
        if (isDebug.isSelected() && Breakpoint.isSelected()) {
            JOptionPane.showMessageDialog(this, "Selecciona solo una opción");
            return;
        }

        else if (Breakpoint.isSelected()) {
            pasoPaso = false;
            Hilo.stop();
            this.Arbol = new ArbolEjecucion(PanelEjecucion);
            Compilar();
            ArmarAmbientes();
            Hilo = new Thread(this);
            Hilo.start();

        }
        else if (isDebug.isSelected()) {
            pasoPaso = true;
            Hilo.stop();
            this.Arbol = new ArbolEjecucion(PanelEjecucion);
            Compilar();
           ArmarAmbientes();
            Hilo = new Thread(this);
            Hilo.start();
        }
        else {
            Hilo.stop();
            pasoPaso = false;
            this.Arbol = new ArbolEjecucion(PanelEjecucion);
            Compilar();
            ArmarAmbientes();
            Hilo = new Thread(this);
            Hilo.start();
        }

    }//GEN-LAST:event_btnRunActionPerformed

    private void btnDedugActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDedugActionPerformed
        stop = true;
        Arbol.setPanelArbol(PanelEjecucion);
        Arbol.ver();
        PanelEjecucion.setArbolEjecucion(Arbol);
        PanelEjecucion.setOk(true);
        PanelEjecucion.repaint();
        /* this.setPasoPaso(true);
        if (this.primeraVez) {
            Compilar();
            Hilo.start();
            this.primeraVez = false;

        }
        else {
            this.stop = true;
        }
         */
        //this.Arbol.ver();
    }//GEN-LAST:event_btnDedugActionPerformed

    private void btnArbolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnArbolActionPerformed
        ArmarArbol();
    }//GEN-LAST:event_btnArbolActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        guardarArchivo();
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnTokensActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTokensActionPerformed
        VisualizarTokens();
    }//GEN-LAST:event_btnTokensActionPerformed

    private void btnErroresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnErroresActionPerformed
        //System.out.println(ContadorLineas);
        
       String a ="";
        for (int i: ContadorLineas.keySet()) {
            a+=" Linea; "+i+" Cantidad\n"+ContadorLineas.get(i);
        }
        JOptionPane.showMessageDialog(this, a);
    }//GEN-LAST:event_btnErroresActionPerformed

    private void isDebugActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_isDebugActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_isDebugActionPerformed

    private void MenuGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuGActionPerformed
        MenuGraficas();
    }//GEN-LAST:event_MenuGActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        ConfugureBreak();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed


    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        Permitir = !Permitir;
    }//GEN-LAST:event_jButton2ActionPerformed
    public void MenuGraficas() {
        JDialog MenuG = new JDialog();
        MenuG.getContentPane().setBackground(color.black);
        MenuG.setBounds(0, 0, 300, 150);
        MenuG.setLayout(null);
        MenuG.setTitle("Tools Estadisticas");

        JButton TomarMuestra = new JButton();
        TomarMuestra.setText("Tomar Muestra");
        TomarMuestra.setBounds(20, 20, 100, 30);

        JButton VerGrafica = new JButton();
        VerGrafica.setText("Ver Estaditica");
        VerGrafica.setBounds(20, 60, 100, 30);

        MenuG.add(VerGrafica);
        MenuG.add(TomarMuestra);
        TomarMuestra.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nombre = JOptionPane.showInputDialog("Ingrese Algoritmo", "");
                int tamaño = Integer.parseInt(JOptionPane.showInputDialog("Ingrese identificador de Muestra", ""));
                if (buscar(nombre)) {
                    for (String a : Informacion.keySet()) {
                        if (a.equalsIgnoreCase(nombre)) {
                            tiempo = TFin - TInicio;
                            int val = (int) tiempo;
                            Informacion.get(nombre).put(tamaño, new Point(sumarLineas(), val));
                        }
                    }
                }
                else {
                    tiempo = TFin - TInicio;
                    int val = (int) tiempo;
                    Informacion.put(nombre, new TreeMap<Integer, Point>());
                    Informacion.get(nombre).put(tamaño, new Point(sumarLineas(), val));
                }

            }
        });
        VerGrafica.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String Nombre = JOptionPane.showInputDialog("Ingrese nombre del Algoritmo que queires graficar", "");
                System.out.println(Informacion);
                Estadisticas Graficas = new Estadisticas(Informacion);
                Graficas.Graficar(Nombre);
                Graficas.Mostrar1();
                Graficas.Mostrar2();
            }
        });

        MenuG.setVisible(true);
    }

    public int sumarLineas() {
        int valor = 0;
        for (int i : ContadorLineas.keySet()) {
            valor += ContadorLineas.get(i);
        }
        return valor;
    }

    public boolean buscar(String nombre) {
        for (String a : Informacion.keySet()) {
            if (a.equalsIgnoreCase(nombre)) {
                return true;
            }
        }
        return false;
    }

    public void ConfugureBreak() {
        MenuSimulacion = new JDialog();
        MenuSimulacion.getContentPane().setBackground(color.black);
        MenuSimulacion.setBounds(0, 0, 300, 150);
        MenuSimulacion.setLayout(null);
        MenuSimulacion.setTitle("Tools BreakPoints");

        JButton BorrarTodo = new JButton();
        BorrarTodo.setText("BorrarTodo");
        BorrarTodo.setBounds(20, 20, 100, 30);

        JButton Agregar = new JButton();
        Agregar.setText("AgregarPoint");
        Agregar.setBounds(20, 60, 100, 30);

        MenuSimulacion.add(Agregar);
        MenuSimulacion.add(BorrarTodo);
        BorrarTodo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BreakPoints.removeAll(BreakPoints);
                MenuSimulacion.dispose();
            }
        });

        Agregar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int Numero = Integer.parseInt(JOptionPane.showInputDialog("Ingrese Numero de linea", ""));
                BreakPoints.add(Numero);
                MenuSimulacion.dispose();
                LineP.setBreakPoints(BreakPoints);
            }
        });

        MenuSimulacion.setVisible(true);
    }

    public void ArmarAmbientes() {
        VentanaArbol.dispose();
        Color Fondo = Color.decode("#f3f3ea");
        PanelEjecucion = new PanelArbol();
        PanelEjecucion.setBackground(Fondo);
        PanelEjecucion.setSize(1000, 730);
        VentanaArbol = new Scroller(PanelEjecucion);
        VentanaArbol.setVisible(true);
        if (Arbol != null) {
            PanelEjecucion.setArbolEjecucion(Arbol);
            PanelEjecucion.repaint();
        }
    }

    public void ArmarArbol() {
        Color Fondo = Color.decode("#f3f3ea");
        if (AnalizadorSintactico.Error != null && Input3 != null) {
            JOptionPane.showMessageDialog(this, "Error-> " + AnalizadorSintactico.Error.getDato() + "  " + AnalizadorSintactico.Error.getLinea());
        }
        if (!AnalizadorSintactico.hayError) {
            PanelArbol panel = new PanelArbol();
            panel.setBackground(Fondo);
            panel.setSize(1000, 730);

            Arbol A = AnalizadorSintactico.ArmarArbol(Input3, panel);

            new Scroller(panel).setVisible(true);

            if (A != null) {
                panel.setDerivacion(A);
                panel.setDerivaconCopia(A);
                panel.repaint();
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        }
        catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Editor.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        }
        catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Editor.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        }
        catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Editor.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        }
        catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Editor.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Editor().setVisible(true);
            }
        });
    }

    private int findLastNonWordChar(String text, int index) {
        while (--index >= 0) {
            if (String.valueOf(text.charAt(index)).matches("\\W")) {
                break;
            }
        }
        return index;
    }

    private int findFirstNonWordChar(String text, int index) {
        while (index < text.length()) {
            if (String.valueOf(text.charAt(index)).matches("\\W")) {
                break;
            }
            index++;
        }
        return index;
    }

    public ArbolEjecucion getArbol() {
        return Arbol;
    }

    public int posicionParado() {
        Element raiz = txt.getDocument().getDefaultRootElement();
        int pos = txt.getCaretPosition();
        return pos;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox Breakpoint;
    private javax.swing.JButton MenuG;
    private javax.swing.JButton btnAbrir;
    private javax.swing.JButton btnArbol;
    private javax.swing.JButton btnDedug;
    private javax.swing.JButton btnErrores;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnId;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnRun;
    private javax.swing.JButton btnTokens;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.JCheckBox isDebug;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JList<String> jList1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JPanel panel;
    // End of variables declaration//GEN-END:variables
    boolean Permitir = false;

    @Override
    public void run() {

        try {
            PartirFuncionesYProcedimientos(ModificarStirng(AnalizadorLexico.GenerarTonkens(txt.getText())));
            GodClass Ejecucion = new GodClass(this.FPExistentes);
            Ejecucion.setInterfaz(this);
            Ejecucion.createScrips();
            if (!AnalizadorSintactico.hayError || Permitir || true) {
                Ejecucion.Run("main");
                codigos = Ejecucion.limpieza();
            }
            else if (AnalizadorSintactico.Error != null && Input3 != null) {
                JOptionPane.showMessageDialog(this, "Error-> " + AnalizadorSintactico.Error.getDato() + " Linea: " + AnalizadorSintactico.Error.getLinea());

            }
        }

        catch (Exception e) {

            JOptionPane.showMessageDialog(this, e.getMessage() + "Error");
            return;

        }
    }

    /**
     * @return the pasoPaso
     */
    public boolean isPasoPaso() {
        return pasoPaso;
    }

    /**
     * @param pasoPaso the pasoPaso to set
     */
    public void setPasoPaso(boolean pasoPaso) {
        this.pasoPaso = pasoPaso;
    }

    public LinkedList<LinkedList<Token>> getFPExistentes() {
        return FPExistentes;
    }

    public void setFPExistentes(LinkedList<LinkedList<Token>> FPExistentes) {
        this.FPExistentes = FPExistentes;
    }

    public PanelArbol getPanelEjecucion() {
        return PanelEjecucion;
    }

    public void setPanelEjecucion(PanelArbol PanelEjecucion) {
        this.PanelEjecucion = PanelEjecucion;
    }

    public void ActualizaeLinea() {
        LineP.linea = this.linea;
        try {
            LineP.resetHighlight1();
        }
        catch (Exception e) {
        }

    }

    public LinePainter getLineP() {
        return LineP;
    }

    public void setLineP(LinePainter LineP) {
        this.LineP = LineP;
    }

}
